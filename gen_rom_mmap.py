#! /usr/bin/env python3
# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
#   Author           Date       Version comments
#   Pieter Donker    June 2020  first version
#   Eric Kooistra    March 2021 support updated mmap format to suit the
#                               register map for lofar2_unb2b_beamformer
#                    June 2021  Use lofar2_unb2b_sdp_station.mmap.gold
#                               instead of lofar2_unb2b_beamformer.mmap.gold
#
###############################################################################

"""
    Generate FPGA memory map (MMAP) file to copy in rom_system
    [fpga_name].mmap. The file for the ROM is selected in radiohdl/run_reg
    script.

    Usage:
    > gen_rom_mmap.py -h
    > gen_rom_mmap.py -d unb2b_minimal
    > gen_rom_mmap.py -d unb2b_minimal -v DEBUG
    > gen_rom_mmap.py -d lofar2_unb2b_filterbank -r lofar2_unb2b_filterbank_full
    > gen_rom_mmap.py -d lofar2_unb2b_filterbank -r lofar2_unb2b_filterbank_full -a  # use base addresses from qsys

    # mmap for 'mother' design
    > gen_rom_mmap.py -d lofar2_unb2b_sdp_station     # use calculated base addresses, for test
    > gen_rom_mmap.py -d lofar2_unb2b_sdp_station -a  # use base addresses from qsys, for on HW

    # same mmap as for 'mother' design, but applied for revision in build/
    > gen_rom_mmap.py -d lofar2_unb2b_sdp_station -r lofar2_unb2b_sdp_station_bf     # use calculated base addresses, for test
    > gen_rom_mmap.py -d lofar2_unb2b_sdp_station -r lofar2_unb2b_sdp_station_bf -a  # use base addresses from qsys, for on HW

    Checks (redo same checks for both unb2b and unb2c):
    1) Run check for expected MMAP in hdl/ repository:
    > . ./init_hdl
    > gen_rom_mmap.py -d lofar2_unb2b_sdp_station
    > gen_rom_mmap.py -d lofar2_unb2c_sdp_station

    View and verify result:
    > more build/unb2b/args/lofar2_unb2b_sdp_station/c/lofar2_unb2b_sdp_station.mmap
    > more build/unb2c/args/lofar2_unb2c_sdp_station/c/lofar2_unb2c_sdp_station.mmap
    > diff build/unb2b/args/lofar2_unb2b_sdp_station/c/lofar2_unb2b_sdp_station.mmap applications/lofar2/designs/lofar2_unb2b_sdp_station/lofar2_unb2b_sdp_station.mmap.gold
    > diff build/unb2c/args/lofar2_unb2c_sdp_station/c/lofar2_unb2c_sdp_station.mmap applications/lofar2/designs/lofar2_unb2c_sdp_station/lofar2_unb2c_sdp_station.mmap.gold

    If there are differences, then a GUI diff viewer like 'meld' is needed to
    more clearly view the differences. If the register map is changed, then
    correct these or if they are expected then update the reference mmap file
    in git:

    > cp build/unb2b/args/lofar2_unb2b_sdp_station/c/lofar2_unb2b_sdp_station.mmap applications/lofar2/designs/lofar2_unb2b_sdp_station/lofar2_unb2b_sdp_station.mmap.gold
    > cp build/unb2c/args/lofar2_unb2c_sdp_station/c/lofar2_unb2c_sdp_station.mmap applications/lofar2/designs/lofar2_unb2c_sdp_station/lofar2_unb2c_sdp_station.mmap.gold

    Now 'git diff' or 'gitmeld' (alias for 'git difftool -t meld --no-prompt')
    can be used to view the diff.

    Then add and commit lofar2_unb2b_sdp_station.mmap.gold to keep it as a
    test case for verificaton of changes in ARGS code, changes in
    lofar2_unb2b_sdp_station.yaml and/or or changes in the MMAP format.

    2) Run the similar check for expected MMAP again, but now using address
       map from QSYS:
    > gen_rom_mmap.py -d lofar2_unb2b_sdp_station -a

    The address map is obtained from the <key>data_master</key> section in:

      applications/lofar2/designs/lofar2_unb2b_sdp_station/quartus/qsys_lofar2_unb2b_sdp_station.qsys

    For mmap and mmap.qsys only the base addresses column (col6) differs.
"""

import sys
import os
import logging
from argparse import ArgumentParser
from args_logger import MyLogger
from py_args_lib import FPGA, RAM, FIFO, Register, PeripheralLibrary, FPGALibrary, MM_BUS_SIZE
import pprint
from common_func import ceil_pow2, ceil_div, smallest


def _make_mask(mm_width, bit_offset=0, user_width=0, user_word_order='le'):
    """Derive MM word bit mask and optional user word bit mask in case it differs from the MM word bit mask.

       Remarks:
       - To ease parsing, use [hi:lo] format also for [i:i] = [i], because the bit mask then always contain a colon ':'.
       - For user_width > mm_width use one user_mask string per MM word
    """
    if user_width == None or user_width == mm_width:
        # user_mask = mm_mask, then use redundant user_mask string
        _mm_mask_strings = ['b[{}:{}]'.format(bit_offset + mm_width - 1, bit_offset)]
        _user_mask_strings = ['-']
    else:
        # user_mask != mm_mask, then force bit_offset = 0
        if user_width < mm_width:
            # pack multiple user words per MM word
            _mm_mask_strings = ['b[{}:{}]'.format(mm_width - 1, 0)]
            _user_mask_strings = ['b[{}:{}]'.format(user_width - 1, 0)]
        else:
            # use seperate user_mask string per MM word
            _mm_mask_strings = []
            _user_mask_strings = []
            _wi = 0
            _user_lo = 0
            while _user_lo < user_width:
                _user_hi = smallest((_wi + 1) * mm_width - 1, user_width - 1)
                _mm_hi = _user_hi % mm_width
                _mm_mask_strings.append('b[{}:{}]'.format(_mm_hi, 0))
                _user_mask_strings.append('b[{}:{}]'.format(_user_hi, _user_lo))
                _wi += 1
                _user_lo = _wi * mm_width
            # default is little endian word order, swap for big endian word order
            if user_word_order == 'be':
                _mm_mask_strings.reverse()
                _user_mask_strings.reverse()
    return _mm_mask_strings, _user_mask_strings


def gen_fpga_map(fpga, fpga_name, rev_name):
    _map_str = []

    # pprint.pprint(fpga.address_map)
    print("Including MM ports for {}:".format(fpga_name))
    map_format_str = '  {:40s}  {:4s}  {:4s}  {:5s}  {:40s}  0x{:08x}  {:>6s}  {:>5s}  {:>11s}  {:>10s}  {:>10s}  {:5s}  {:5s}'
    map_nof_columns = len(map_format_str.split('{')) - 1
    for mm_port_name, mm_port_info in fpga.address_map.items():

        # All elements in array have same info, so only need info from first element
        if mm_port_info['periph_num'] > 0:
            continue

        mm_port       = mm_port_info['mm_port']
        user_def_name = mm_port.user_defined_name().upper()

        number_of_peripherals = mm_port_info['peripheral'].number_of_peripherals()
        number_of_mm_ports    = mm_port.number_of_mm_ports()
        base                  = int(mm_port_info['base'])
        base_word             = int(base / MM_BUS_SIZE)

        mm_peripheral_span = '-'
        mm_port_span = '-'
        
        if number_of_mm_ports > 1 or number_of_peripherals > 1:
            port_span       = mm_port.mm_port_span() // MM_BUS_SIZE         # evaluated mm_port_span from key in peripheral.yaml
            #port_span       = mm_port.address_length() // MM_BUS_SIZE       # minimal mm_port_span derived from last field address offset in mm_port

            mm_port_span       = str(port_span)

        if number_of_peripherals > 1:
            peripheral_span = mm_port_info['peripheral'].peripheral_span() // MM_BUS_SIZE  # evaluated peripheral_span from key in fpga.yaml
            #peripheral_span = ceil_pow2(number_of_mm_ports) * port_span     # minimal peripheral_span, always a power of 2
            #peripheral_span = mm_port_info['span'] // MM_BUS_SIZE           # original way to get peripheral_span, not always a power of 2

            mm_peripheral_span = str(peripheral_span)

        number_of_peripherals = str(number_of_peripherals)
        number_of_mm_ports = str(number_of_mm_ports)

        if isinstance(mm_port, RAM):
            print(' RAM  {:85s} at 0x{:08x}(bytes) 0x{:04x}(words)  "{}"'.format(mm_port_name, base, base_word, user_def_name))

            #print('{}: {}, {}, {}'.format(field_name, f.radix().lower(), f.user_width(), f.resolution_w()))

            # EK TODO:: 'RAM' object has no attribute 'fields', it should have because RAM and FIFO are no so different than REG, or there is only
            #           one field, so therefore no need for fields level. RAM , FIFO are a Field, REG uses one or more Field

            #print('{}: {}, {}, {}'.format(mm_port.name(), mm_port.number_of_fields(), mm_port.mm_width(), mm_port.user_width()))

            number_of_fields = str(mm_port.number_of_fields())

            radix = mm_port.radix()
            mm_masks, user_masks = _make_mask(mm_port.mm_width(), mm_port.bit_offset(), mm_port.user_width(), mm_port.user_word_order())

            _map_str.append(map_format_str.format(
                            user_def_name,
                            number_of_peripherals,
                            number_of_mm_ports,
                            'RAM',
                            'data',   # EK: TODO:should come from yaml field_name
                            base_word,
                            number_of_fields,
                            mm_port.access_mode(),
                            radix,
                            mm_masks[0],
                            user_masks[0],
                            mm_peripheral_span,
                            mm_port_span
                            ))

            # write second MM word incase user_width > mm_width
            for wi in range(1, len(user_masks)):
                _map_str.append(map_format_str.format(
                                '-',
                                '-',
                                '-',
                                '-',
                                '-',
                                base_word + wi,
                                '-',
                                '-',
                                '-',
                                mm_masks[wi],
                                user_masks[wi],
                                '-',
                                '-'
                                ))

        elif isinstance(mm_port, FIFO):
            print(' FIFO {:85s} at 0x{:08x}(bytes) 0x{:04x}(words)  "{}"'.format(mm_port_name, base, base_word, user_def_name))

            number_of_fields = str(mm_port.number_of_fields())

            radix = mm_port.radix()
            mm_masks, user_masks = _make_mask(mm_port.mm_width(), mm_port.bit_offset(), mm_port.user_width(), mm_port.user_word_order())

            _map_str.append(map_format_str.format(
                            user_def_name,
                            number_of_peripherals,
                            number_of_mm_ports,
                            'FIFO',
                            'data',   # EK: TODO:should come from yaml field_name
                            base_word,
                            number_of_fields,
                            mm_port.access_mode(),
                            radix,
                            mm_masks[0],
                            user_masks[0],
                            mm_peripheral_span,
                            mm_port_span
                            ))

            # write second MM word incase user_width > mm_width
            for wi in range(1, len(user_masks)):
                _map_str.append(map_format_str.format(
                                '-',
                                '-',
                                '-',
                                '-',
                                '-',
                                base_word + wi,
                                '-',
                                '-',
                                '-',
                                mm_masks[wi],
                                user_masks[wi],
                                '-',
                                '-'
                                ))

        elif isinstance(mm_port, Register):
            mm_port_type       = "REG"   # EK: TODO:get mm_port_type from mm_port ?
            print(' REG  {:85s} at 0x{:08x}(bytes) 0x{:04x}(words)  "{}"'.format(mm_port_name, base, base_word, user_def_name))

            done = []
            for f in mm_port.fields:
                field_name = f.name()

                #print('{}: {}, {}, {}'.format(field_name, f.radix().lower(), f.user_width(), f.resolution_w()))

                # EK: TODO:the check on f.number_of_fields() and on done should not be necessary, because the array of fields should only have one element in mm_port.fields. The mm_port.number_of_fields() should not have been expanded in mm_port.
                if f.number_of_fields() > 1:
                    field_name = f.name()
                if field_name in done:
                    continue
                if field_name not in done:
                    done.append(field_name)

                #field_group = f.group_name()
                #if field_group == None:
                #    field_group = '-'

                f_base = base_word + int(f.address_offset() // MM_BUS_SIZE)

                number_of_fields = str(f.number_of_fields())

                # EK: TODO:support parameter passing for user_width, now the parameter string seems to be passed instead of the parameter evaluation
                # EK: TODO:support default user_width = None --> user_width = mm_width
                #print('{}: {}, {}, {}'.format(field_name, f.mm_width(), f.user_width(), f.bit_offset()))
                mm_masks, user_masks = _make_mask(f.mm_width(), f.bit_offset(), f.user_width(), f.user_word_order())

                # write field info and first MM word
                _map_str.append(map_format_str.format(
                                user_def_name,
                                number_of_peripherals,
                                number_of_mm_ports,
                                mm_port_type,
                                field_name,
                                f_base,
                                number_of_fields,
                                f.access_mode(),
                                f.radix(),
                                mm_masks[0],
                                user_masks[0],
                                mm_peripheral_span,
                                mm_port_span
                                ))

                # write second MM word incase user_width > mm_width
                for wi in range(1, len(user_masks)):
                    _map_str.append(map_format_str.format(
                                    '-',
                                    '-',
                                    '-',
                                    '-',
                                    '-',
                                    f_base + wi,
                                    '-',
                                    '-',
                                    '-',
                                    mm_masks[wi],
                                    user_masks[wi],
                                    '-',
                                    '-'
                                    ))

                # only log table entry for first field of MM port
                user_def_name = '-'
                number_of_peripherals = '-'
                number_of_mm_ports = '-'
                mm_port_type = '-'
                mm_peripheral_span = '-'
                mm_port_span = '-'

    _map_info = []
    _map_info.append('fpga_name = {}'.format(fpga_name))  # identifies <fpga_name>.fpga.yaml
    _map_info.append('number_of_columns = {}'.format(map_nof_columns))  # to support mmap with more columns in future
    _map_info.append('# There can be multiple lines with a single key. The host should ignore unknown keys.')
    _map_info.append('# The lines with columns follow after the number_of_columns keys. The host should ignore')
    _map_info.append('# the extra columns in case the mmap contains more columns than the host expects.')
    _map_info.append('#')
    _map_info.append('# col 1: mm_port_name, if - then it is part of previous MM port.')
    _map_info.append('# col 2: number of peripherals, if - then it is part of previous peripheral.')
    _map_info.append('# col 3: number of mm_ports, if - then it is part of previous MM port.')
    _map_info.append('# col 4: mm_port_type, if - then it is part of previous MM port.')
    _map_info.append('# col 5: field_name')
    _map_info.append('# col 6: field start address (in MM words)')
    _map_info.append('# col 7: number of fields, if - then it is part of previous field_name.')
    _map_info.append('# col 8: field access_mode, if - then it is part of previous field_name.')
    _map_info.append('# col 9: field radix, if - then it is part of previous field_name.')
    _map_info.append('# col 10: field mm_mask')
    _map_info.append('# col 11: field user_mask, if - then it is same as mm_mask')
    _map_info.append('# col 12: mm_peripheral_span (in MM words), if - then the span is not used or already defined on first line of MM port')
    _map_info.append('# col 13: mm_port_span (in MM words), if - then the span is not used or already defined on first line of MM port')

    _map_info.append('#')
    _map_info.append('# col1                                      col2  col3  col4   col5                                      col6        col7    col8   col9         col10       col11       col12  col13')
    _map_info.append('# ----------------------------------------  ----  ----  -----  ----------------------------------------  ----------  ------  -----  -----------  ----------  ----------  -----  -----')

    out_dir = os.path.join(os.getenv('ARGS_BUILD_DIR'), fpga.board_name.replace('uniboard', 'unb'), 'args', rev_name, 'c')
    try:
        os.stat(out_dir)
    except FileNotFoundError:
        os.makedirs(out_dir)

    with open(os.path.join(out_dir, '{}.mmap'.format(rev_name)), 'w') as file:
        file.write('\n'.join(_map_info))
        file.write('\n')
        file.write('\n'.join(_map_str))


if __name__ == '__main__':

    parser = ArgumentParser(description='ARGS tool script to generate fpgamap.py M&C Python client include file')
    parser.add_argument('-d', '--design', required=True, help='ARGS fpga_name')
    parser.add_argument('-r', '--revision', default="", help='HDL revision_name')
    parser.add_argument('-a', '--avalon', action='store_true', default=False, help="use config file qsys/sopc for base addresses")
    parser.add_argument('-v', '--verbosity', default='INFO', help="stdout log level can be [ERROR | WARNING | INFO | DEBUG]")
    args = parser.parse_args()

    fpga_name = args.design
    rev_name = args.design if args.revision == "" else args.revision
    use_avalon = args.avalon

    libRootDir = os.path.expandvars('$ARGS_WORK')

    # setup first log system before importing other user libraries
    program_name = __file__.split('/')[-1].split('.')[0]
    out_dir = os.path.join(os.getenv('ARGS_GEAR'), 'log')
    my_logger = MyLogger(log_path=out_dir, file_name=program_name)
    my_logger.set_file_log_level('DEBUG')
    my_logger.set_stdout_log_level(args.verbosity)

    logger = my_logger.get_logger()
    logger.debug("Used arguments: {}".format(args))

    # Find and parse all *.fpga.yaml YAML files under libRootDir
    # For each FPGA file it extracts a list of the peripherals that are used by that FPGA
    # Here we select the FPGA YAML that matches up with the supplied fpga command line argument
    fpga_lib = FPGALibrary(root_dir=libRootDir, use_avalon_base_addr=use_avalon)
    fpga = fpga_lib.get_fpga(fpga_name)

    gen_fpga_map(fpga, fpga_name, rev_name)
