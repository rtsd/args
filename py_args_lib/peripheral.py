
# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author  Date
#   HJ    jan 2017  Original
#   EK    feb 2017
#   PD    feb 2017
#   PD    feb 2019
#   PD    feb 2020, cleanup and update for only byte adresses
#
###############################################################################


import os
from copy import deepcopy
from math import ceil
import logging
import yaml
import collections
from peripheral_lib import *
from common_func import ceil_div

logger = logging.getLogger('main.peripheral')

# EK TODO:better put class Peripheral and class PeripheralLibrary in separate files?
# EK TODO:better put class PeripheralLibrary and class FPGALibrary in same file? --> No

class Peripheral(BaseObject):
    """ A Peripheral consists of one or more MM ports. The MM port can be a
    Register, RAM or FIFO.

    The Peripheral has parameters to configure the MM ports.

    A Peripheral has two levels of supporting multiple instances:
    1) At FPGA level a peripheral can be used more than once. The individual
       peripherals are then distinghuised by defining a unique pelabel per
       instance.
    2) At peripheral level internally in the peripheral if the peripheral
       support an array of instances. The size of the array is then specified
       via nof_inst. Dependent on the specific definition of the peripheral,
       the nof_inst then either replicates some fields in a register or it
       replicates some or all MM ports nof_inst times.

    The Peripheral evaluates the nof_inst and parameters to set the
    dimensions of the MM ports.
    """
    def __init__(self, library_config):
        super().__init__()
        self._config         = {}  # read config from file
        self._parameters     = {}  # all used parameters
        # self.registers       = {}  # all used registers
        # self.rams            = {}  # all used rams
        # self.fifos           = {}  # all used fifos
        self.mm_ports          = []
        self._component_name = library_config['peripheral_name']
        self.name(self._component_name)
        self._valid_keys = ['number_of_peripherals']
        self._args.update({'number_of_peripherals' : DEFAULT_NUMBER_OF_PERIPHERALS,
                           'peripheral_span'       : DEFAULT_MM_PERIPHERAL_SPAN})
        self.evaluated = False
        self._config         = library_config  # [self.component_name()]
        self.reg_len = 0

        logger.debug("extract config for %s", self.component_name())
        self.extract_config()

    def get_mm_port(self, name):

        for mmport in self.mm_ports:
            if name == mmport.name() or mmport.user_defined_name():
                return mmport
        return None

    def component_name(self):
        """ get component_name """
        return self._component_name

    def number_of_peripherals(self, val=None):
        """ set/get number of peripherals """
        if val is not None:
            self.set_kv('number_of_peripherals', val)
            return
        return self._as_int('number_of_peripherals')

    def peripheral_span(self, val=None):
        """ set/get number of peripherals """
        if val is not None:
            self.set_kv('peripheral_span', val)
            return
        return self._as_int('peripheral_span')

    def parameter(self, key, val=None):

        if '.' in key:
            _struct, _key = key.split('.')
            if _struct not in self._parameters:
                logger.error("key '%s' not in parameters", key)
                return None
            if _key not in self._parameters[_struct]:
                logger.error("key '%s' not in parameters", key)
                return None
            if val is not None:
                logger.debug("  Parameter %s default value: %s is overwritten with new value: %s",
                             key,
                             str(self._parameters[_struct][_key]),
                             str(val))
                self._parameters[_struct][_key] = val
                return

        if val is not None:
            logger.debug("  Parameter %s default value: %s is overwritten with new value: %s",
                         key,
                         str(self._parameters.get(key, 'None')),
                         str(val))
            self._parameters[key] = val
            return
        return self._parameters[key]

    def set_user_defined_mm_port_name(self, mm_port_nr, name):
        """ Set user defined mm_port_name
        """
        if mm_port_nr in range(len(self.mm_ports)):
            self.mm_ports[mm_port_nr].user_defined_name(name)

    def extract_config(self):
        """ extract all kv pairs from the config (yaml file)
        and assign it to the following dicts:
        parameters, registers, rams, fifos """

        # clear all existing settings
        self._parameters = {}
        # self.registers  = {}
        # self.rams       = {}
        # self.fifos      = {}

        if "parameters" in self._config:
            # self.parameters.update(val)

            _parameters = deepcopy(self._config['parameters'])

            # keys with '.' in the name indicate the use of a structs
            # inside this class it is a dict in a dict
            for parameter_set in _parameters:
                # print(parameter_set)
                name  = parameter_set['name']
                value = parameter_set['value']
                if '.' in name:
                    _struct, _name = name.split('.')
                    if _struct not in self._parameters:
                        self._parameters[_struct] = {}
            # structs available now in self._parameters

            remove_list = []
            for parameter_set_nr, parameter_set in enumerate(_parameters):
                name  = parameter_set['name']
                value = parameter_set['value']
                if '.' in name:  # struct notation with dot
                    _struct, _name = name.split('.')
                    self._parameters[_struct][_name] = self._eval(str(value))
                else:
                    self._parameters[name] = self._eval(str(value))
                remove_list.append((parameter_set_nr, name))

            for parameter_set_nr, name in sorted(remove_list, reverse=True):
                logger.debug("delete '%s' from parameters", name)
                del _parameters[parameter_set_nr]

            for parameter_set in _parameters:
                name  = parameter_set['name']
                value = parameter_set['value']
                logger.debug("eval of name=%s and value=%s not posible", name, value)

            logger.debug("used parameters=%s", str(self._parameters))

        if 'mm_ports' in self._config:
            mm_ports = deepcopy(self._config['mm_ports'])

            if not isinstance(mm_ports, list):
                logger.error("mm_ports not a list in *.peripheral.yaml")
                sys.exit()

            for mm_port_nr, mm_port_info in enumerate(mm_ports):
                logger.debug("mm_port_name=%s mm_port_type=%s", mm_port_info.get('mm_port_name', None), mm_port_info.get('mm_port_type', None))
                mm_port_name = mm_port_info['mm_port_name']

                if mm_port_name is None:
                    logger.error("Peripheral '%s': 'mm_port_name' key missing value in *.peripheral.yaml", self.name())
                    sys.exit()

                i = 0
                if mm_port_info.get('mm_port_type', '').upper() in VALID_MM_PORT_TYPES:
                   
                    fields = []
                    if 'fields' in mm_port_info:
                        defaults = {}
                        for field_group in mm_port_info['fields']:
                            if isinstance(field_group, dict):  # labelled field group
                                (group_label, v) = field_group.popitem()
                                field_group = v
                            elif len(field_group) > 1:          # unlabelled field group
                                group_label = "reg{}".format(i)
                                i = i + 1
                            else :
                                group_label = None  # only one field in group

                            logger.debug('group_label=%s', str(group_label))

                            for field_info in field_group:

                                # get defaults dictionary if exists for field group
                                if field_info.get('field_defaults', None) is not None:
                                    defaults = field_info['field_defaults']
                                    if any([key.lower() not in VALID_DEFAULT_KEYS for key in defaults.keys()]) :
                                        defaults = {}
                                        logger.error("{}.peripheral.yaml: Invalid key set in defaults for field group {}. Valid keys are {}".format(self.lib, group_label, VALID_DEFAULT_KEYS))
                                        sys.exit()
                                    continue

                                field_name = field_info['field_name']
                                logger.debug("field_info= %s", str(field_info))
                                try :
                                    field = Field(field_name, field_info)
                                except ARGSNameError:
                                    logger.error("Invalid name '%s' for field in *.peripheral.yaml", field_name)
                                    sys.exit()
                                if group_label is not None :
                                    field.group_name(group_label)
                                if field_name == "args_map_build":
                                    logger.info("args_map_build = {}".format(Peripheral.__timestamp))
                                    field_info['reset_value'] = Peripheral.__timestamp
                                for key, val in field_info.items():
                                    if val is not None:
                                        if key == 'field_name':
                                            continue
                                        if key.lower() in VALID_FIELD_KEYS:  # if valid attribute key, apply value to attribute
                                            eval("field.{}(val)".format(key.lower()))
                                        else:
                                            logger.error("Unknown key %s in *.peripheral.yaml", key)
                                            sys.exit()

                                    else:
                                        logger.error("Peripheral '%s': MM port '%s': '%s' key missing value in *.peripheral.yaml",
                                                     self.name(), mm_port_name, key)
                                        sys.exit()

                                for key, val in defaults.items():
                                    if field_info.get(key, None) is None:  # if key doesn't exist in config file, apply defaults
                                        eval("field.{}(val)".format(key.lower()))
                                        logger.debug("*.peripheral.yaml: Setting field %s key %s to default %s",
                                                     field_info['field_name'], str(key), str(val))

                                if field.success:
                                    fields.append(deepcopy(field))
                                else:
                                    logger.error("*.peripheral.yaml: field '%s' not succesfully added to fields", field_name)
                                    sys.exit()

                    if mm_port_info['mm_port_type'].upper() in ['RAM', 'FIFO']:
                        field = field_group[0]
                        if mm_port_info['mm_port_type'] in ['RAM']:
                            self.add_ram(mm_port_nr, mm_port_name, field)
                        else:
                            self.add_fifo(mm_port_nr, mm_port_name, field)
                    else:  # mm_port_type is REG or REG_IP
                        logger.debug('adding register %s\n', mm_port_name)
                        self.add_register(mm_port_nr, mm_port_name, fields)

                    if 'number_of_mm_ports' in mm_port_info.keys():
                        self.mm_ports[-1].update_args({'number_of_mm_ports': mm_port_info['number_of_mm_ports']})
                    if 'mm_port_span' in mm_port_info.keys():
                        self.mm_ports[-1].update_args({'mm_port_span': mm_port_info['mm_port_span']})    
                    if 'mm_port_description' in mm_port_info.keys():
                        self.mm_ports[-1].update_args({'mm_port_description': mm_port_info['mm_port_description']})
                    if 'dual_clock' in mm_port_info.keys():
                        self.mm_ports[-1].update_args({'dual_clock': mm_port_info['dual_clock']})

                else :
                    logger.error("Peripheral '%s': MM port '%s': Invalid value %s for 'mm_port_type' key in *.peripheral.yaml",
                                 self.name(), mm_port_name, mm_port_info.get('mm_port_type', 'None'))
                    sys.exit()

        if 'peripheral_description' in self._config:
            self.update_args({'peripheral_description': self._config['peripheral_description']})

    def _eval(self, val, val_type=None):
        """ evaluate val.
        1) trying to parse values of known parameter into the value
        2) eval the value, known imported function are also evaluated """
        logger.debug("eval val %s", str(val))
        _val = str(val)
        # first replace all knowns parameter names with its assigned value
        for key1, val1 in self._parameters.items():
            # if val is a dict, in vhdl it's a struct
            if isinstance(val1, dict):
                for key2, val2 in val1.items():
                    key = "{}.{}".format(key1, key2)
                    if key in _val:
                        logger.debug("replace %s with %s", key, str(val2))
                        _val = _val.replace(key, str(val2))
            else:
                if key1 in _val:
                    logger.debug("replace %s with %s", key1, str(val1))
                    _val = _val.replace(key1, str(val1))
        if val is None:
            logger.error("key set to invalid value %s in *.peripheral.yaml", _val)
            sys.exit()

        if '.coe' in _val:
            return _val
        try:
            result = eval(_val)
            if isinstance(result, float):
                result = int(result)
        except SyntaxError:
            logger.error("Key set to invalid value '%s' in *.peripheral.yaml", _val)
            sys.exit()
        except NameError:
            result = _val
            if val_type == int:
                logger.error("Key set to invalid value '%s' in *.peripheral.yaml", _val)
                logger.error("Is parameter defined?")
                sys.exit()

        logger.debug("  _eval(%s) returns eval(%s) = %s", str(val), _val, str(result))

        return result

    def add_parameter(self, name, value):
        """ add parameter to  peripheral
        """
        self._parameters[name] = value

    def add_register(self, mm_port_nr, name, fields):
        """ add register to peripheral
        """
        logger.debug("add register name is %s", name)
        register = deepcopy(self.init_mm_port('Register', name, fields))
        # self.registers['mm_port_{}'.format(mm_port_nr)] = register
        self.mm_ports.append(register)

    def add_ram(self, mm_port_nr, name, settings):
        """ add RAM to peripheral
        """
        logger.debug("add ram name is %s", name)
        ram = deepcopy(self.init_mm_port('RAM', name, settings))
        # self.rams['mm_port_{}'.format(mm_port_nr)] = ram
        self.mm_ports.append(ram)

    def add_fifo(self, mm_port_nr, name, field):
        """ add FIFO to peripheral """
        logger.debug("add fifo name is %s", name)
        fifo = deepcopy(self.init_mm_port('FIFO', name, field))
        # self.fifos['mm_port_{}'.format(mm_port_nr)] = fifo
        self.mm_ports.append(fifo)

    def init_mm_port(self, mm_port_type, name, settings):
        """ init MM port based on type with error checking """
        add_mm_port = "{}(name, settings)".format(mm_port_type)
        try :
            mmport = eval(add_mm_port)
        except ARGSNameError:
            logger.error("Invalid mm_port_name '%s' for %s in *.peripheral.yaml", name, mm_port_type)
            sys.exit()
        except ARGSModeError:
            logger.error("Invalid access mode for %s '%s' in *.peripheral.yaml", mm_port_type, name)
            sys.exit()
        return mmport

    def eval_fields(self, fields):
        """ Evaluate the fields """

        for field in fields:  # .values():
            logger.debug("eval field %s", field.name())
            if [(field.name() == _field.name() and field.group_name() == _field.group_name()) for _field in fields].count(True) > 1:
                logger.error("Field name '%s' group_name '%s' is not unique within MM port field list in *.peripheral.yaml",
                             field.name(), field.group_name())
                sys.exit()
            if field.group_name() is not None:
                field.name(field.group_name() + '_' + field.name())
            field.mm_width(val=self._eval(field.mm_width(), int))
            if field.user_width() == None:
                field.user_width(field.mm_width())  # EK: TODO:why can self._eval(field.user_width(), int)) not work for None ?
            field.user_width(val=self._eval(field.user_width(), int))   # EK TODO:why is there no _eval for every field key?
            field.bit_offset(val=self._eval(field.bit_offset(), int))
            field.access_mode(val=self._eval(field.access_mode()))
            field.side_effect(val=self._eval(field.side_effect()))
            field.address_offset(val=self._eval(field.address_offset(), int))
            field.number_of_fields(val=self._eval(field.number_of_fields(), int))
            field.reset_value(val=self._eval(field.reset_value(), int))
            field.radix(val=self._eval(field.radix()))
        logger.debug("done eval")

    def eval_fifo(self):
        """ Evaluate the paramters and the nof_inst of the peripheral  """

        for mmport in self.mm_ports:
            if not isinstance(mmport, FIFO):
                continue
                # Evaluate the fields
            logger.debug("eval fifo")
            fifo = mmport
            self.eval_fields([fifo])
            fifo.number_of_mm_ports(val=self._eval(fifo.number_of_mm_ports()))
            fifo.mm_port_span(val=self._eval(fifo.mm_port_span()))
            logger.debug("  -FIFO depth (fields): %s", fifo.number_of_fields())
            fifo.address_length(val=ceil_pow2(self._eval(fifo.number_of_fields())) * MM_BUS_SIZE)  # address_length in bytes
            logger.debug("  -FIFO depth (bytes): %s", fifo.address_length())
            logger.debug("  -FIFO width (bits): %s", fifo.mm_width())

    def eval_ram(self):
        """Evaluate the parameters and the nof_inst of the peripheral in order to define the
           real address_length and width of the RAM.
           For example: address_length = c_nof_weights*c_nof_signal_paths
                        witdh = c_weights_w*c_nof_complex """

        for mmport in self.mm_ports:
            # Evaluate the fields and see if there are field that have to be repeated.
            if not isinstance(mmport, RAM):
                continue

            ram = mmport
            logger.debug("eval ram")
            self.eval_fields([ram])
            #if ram.mm_width() < DEFAULT_FIELD_MM_WIDTH:                # EK: TODO:remove these lines --> better generalize REG, RAM and FIFO
            #    ram.mm_width(DEFAULT_FIELD_MM_WIDTH)
            #if ram.user_mm_width() < DEFAULT_FIELD_MM_WIDTH:
            #    ram.user_mm_width(DEFAULT_FIELD_MM_WIDTH)

            ram.number_of_mm_ports(val=self._eval(ram.number_of_mm_ports()))
            ram.mm_port_span(val=self._eval(ram.mm_port_span()))

        for mmport in self.mm_ports:
            # Evaluate the fields and see if there are field that have to be repeated.
            if not isinstance(mmport, RAM):
                continue

            ram = mmport
            logger.debug("  -RAM depth (fields): %s", ram.number_of_fields())
            # Here the variables are used to evaluate the true value for the depth
            # parameter(taking int account the nof_inst as well)

            # EK: TODO:account for that number_of_fields() now counts fields instead of MM words, anyways the address length calculation should not be done here in the ARGS parser
            #number_of_fields_per_word = 1
            #if ram.user_width() != None:
            #    number_of_fields_per_word = ceil_div(ram.mm_width(), ram.user_width())

            ram.address_length(val=ceil_pow2(self._eval(ram.number_of_fields())) * MM_BUS_SIZE)  # address_length in bytes
            logger.debug("  -RAM depth (bytes): %d", ram.address_length())

            logger.debug("  -RAM width (bits): %s", ram.mm_width())
            # Here the variables are used to evaluate the true value for the width parameter.
            #ram.user_mm_width(val=self._eval(ram.user_width()))                    # EK: TODO:remove these lines --> better generalize REG, RAM and FIFO
            ram.update_address_length()
            logger.debug("  -RAM width eval: %d (bits)", ram.mm_width())
            logger.debug("  %s access_mode: %s", ram.name(), ram.access_mode())
            logger.debug("  %s depth: %d (bytes)", ram.name(), ram.address_length())
            #logger.debug("  %s width: %d (bits)", ram.name(), ram.user_width())         # EK: TODO:remove these lines --> better generalize REG, RAM and FIFO

    def eval_register(self):
        """Evaluate the register address_length based on the evaluation of the fields,
           nof registers and the nof_inst."""

        # logger.debug("Number of registers = %d", len(self.registers.items()))

        for mmport in self.mm_ports:
            if not isinstance(mmport, Register):
                continue

            register = mmport
            logger.debug("evaluate %s", register.name())

            # Evaluate the fields.
            logger.debug("eval register fields")
            self.eval_fields(register.fields)

            # and see if there are field that have to be repeated.
            fields_eval = []
            for field in register.fields:
                fields_eval.append(field)

            logger.debug("eval register")
            register.number_of_mm_ports(val=self._eval(register.number_of_mm_ports()))
            register.mm_port_span(val=self._eval(register.mm_port_span()))

            register_name = []
            if self.prefix() not in (None, ''):
                register_name.append(self.prefix().upper())
            register_name.append(register.name())
            register.name('_'.join(register_name))

            # set base addresses for reg fields implemented as RAM
            base_addr = 0
            for field in register.rams:
                base_addr = ceil(base_addr / (ceil_pow2(field.number_of_fields()) * MM_BUS_SIZE)) * ceil_pow2(field.number_of_fields()) * MM_BUS_SIZE  # lowest possible base_addr
                base_addr = base_addr + ceil_pow2(field.number_of_fields()) * MM_BUS_SIZE * register.number_of_mm_ports()  # new base address
                field.base_address(base_addr)

            # ### Assigned Address and bits to register fields
            # 1st pass for manually set address fields
            occupied_addresses = []
            for field in fields_eval:
                if field.address_offset() != UNASSIGNED_ADDRESS:
                    occupied_addresses.append(field.address_offset())
                    # logger.debug("*.peripheral.yaml: field {} has manually set address {}".format(field.name(), str(hex(field.address_offset()))))

            # 2nd pass for automatic address and bit offset assignment
            lowest_free_addr = 0
            group_address = UNASSIGNED_ADDRESS
            last_group = None
            for field in fields_eval:
                # new field group or single field
                if field.group_name() != last_group or field.group_name() is None:
                    if field.group_name() is not None:
                        field_group = [_field for _field in fields_eval if _field.group_name() == field.group_name()]
                        occupied_bits = [bit for _field in field_group for bit in range(_field.bit_offset(), _field.bit_offset()+_field.mm_width()) if _field.bit_offset() != UNASSIGNED_BIT]
                    else :
                        occupied_bits = list(range(field.bit_offset(), field.bit_offset() + field.mm_width())) if field.bit_offset() != UNASSIGNED_BIT else []

                    while (lowest_free_addr) in occupied_addresses:
                        lowest_free_addr = lowest_free_addr + MM_BUS_SIZE

                    if len(set(occupied_bits)) < len(occupied_bits) or any([bit > (MM_BUS_WIDTH - 1) or bit < 0 for bit in occupied_bits]):
                        logger.error('*.peripheral.yaml: Manually assigned bits for field %s is outside of MM_BUS_WIDTH or contains bit collisions',
                                     field.name() if field.group_name() is None else "group " + field.group_name())
                        logger.error("{}".format(str(occupied_bits)))
                        sys.exit()
                # track beginning of group address
                if field.group_name() is not None:
                    if field.group_name() == last_group:  # for all subsequent fields in field group
                        field.address_offset(group_address)
                    else:  # new group, do for first field in group, checks addresses within group and determines group_address or sets as unassigned
                        last_group = field.group_name()
                        # get unique list of addresses for fields with matching group name and address not equal to unassigned address
                        group_addresses = list(set([_field.address_offset() for _field in field_group if _field.address_offset() != UNASSIGNED_ADDRESS]))
                        if len(group_addresses) == 1:
                            group_address = group_addresses[0]
                        elif len(group_addresses) > 1:
                            logger.error('*.peripheral.yaml: Manually set addresses within field group %s %s are conflicting, please change in configuration file',
                                         field.group_name(), type(field.group_name()))
                            sys.exit()
                        else:  # address not assigned
                            group_address = UNASSIGNED_ADDRESS

                if field.address_offset() == UNASSIGNED_ADDRESS:
                    field.address_offset(lowest_free_addr)
                    occupied_addresses.append(lowest_free_addr)

                if field.bit_offset() == UNASSIGNED_BIT:
                    free_bit = 0
                    while any([i in occupied_bits for i in range(free_bit, free_bit + field.mm_width() + 1)]):  # bit is occupied
                        free_bit = free_bit + 1  # try next bit
                        if free_bit == DEFAULT_FIELD_MM_WIDTH:  # 31 didn't work
                            logger.error('*.peripheral.yaml: No suitable gap available for automatic bit offset assignment of field%s',
                                         field.name())
                            logger.error("Check peripheral.yaml file. Field group may be overstuffed or manual bit assignment may have precluded space for other fields")
                            break
                    field.bit_offset(free_bit)
                occupied_bits = occupied_bits + list(range(field.bit_offset(), field.bit_offset() + field.mm_width()))

            # re-sort fields to be ordered by address and bit offsets
            fields_eval.sort(key=lambda x: x.address_offset())
            sorted_fields = []
            dummy_group = []
            group_address = -1
            for field in fields_eval:
                if field.address_offset() == group_address:
                    continue
                group_address = field.address_offset()
                dummy_group = [field for field in fields_eval if field.address_offset() == group_address]
                sorted_fields.extend(sorted(dummy_group, key=lambda x: x.bit_offset()))
            register.fields = sorted_fields                  # Update the fields with evaluated fields

            register.update_address_length()  # Estimate the new address_length after evaluation of the fields and nof_inst
            logger.info("  %s address_length: %d", register.name(), register.address_length())
            register.base_address(base_addr)
            base_addr = base_addr + register.address_length() * register.number_of_mm_ports()

            self.reg_len = base_addr

    def eval_peripheral(self):
        """Evaluate name, label, nof_inst and the parameters to determine the true size of
           the RAMs and the register width and the name of the peripheral, registers and RAMS """
        logger.debug(" Evaluating peripheral '%s'", self.name())

        self.eval_fifo()
        self.eval_ram()
        self.eval_register()
        logger.debug(" Evaluating peripheral '%s' done", self.name())
        self.evaluated = True

    def get_description(self):
        """ return peripheral description """
        return self._config.get('peripheral_description', "")

    def show_overview(self, header=True):
        """ print system overview
        """
        if header:
            logger.debug("--------------------")
            logger.debug("PERIPHERAL OVERVIEW:")
            logger.debug("--------------------")

        logger.debug("Peripheral name:     %s", self.name())
        if self.number_of_peripherals() > 1:
            logger.debug("  number_of_peripheral_instances=%d", self.number_of_peripherals())
        logger.debug("  RAM and REG:")
        for ram in self.mm_ports:
            if not isinstance(ram, RAM):
                continue
            logger.debug("    %-20s:", ram.name())
            if ram.number_of_mm_ports() > 1:
                logger.debug("      number_of_mm_ports=%-3s", str(ram.number_of_mm_ports()))
            logger.debug("      fields:")
            # first make list with address_offset as first item to print later fields orderd on address.
            fields = []
            fields.append([ram.address_offset(), ram.name()])

            for _offset, _name in sorted(fields):
                field = ram
                logger.debug("        %-20s:", _name)
                logger.debug("          width=%-2s       number_of_fields=%s",
                             str(field.mm_width()), str(field.number_of_fields()))

        for reg in self.mm_ports:
            if not isinstance(reg, Register):
                continue
            logger.debug("    %-20s:", reg.name())
            if reg.number_of_mm_ports() > 1:
                logger.debug("      number_of_mm_ports=%-3s", str(reg.number_of_mm_ports()))
            logger.debug("      address_length=%-3s", str(reg.address_length()))
            logger.debug("      fields:")

            for field in reg.fields:

                logger.debug("        %-20s", field.name())
                logger.debug("          width=%-2s       address_offset=0x%02x  access_mode=%-4s  reset_value=%-4s  radix=%s",
                             str(field.mm_width()), field.address_offset(), field.access_mode(), str(field.reset_value()), field.radix())
                logger.debug("          bit_offset=%-2s  number_of_fields=%-4s  side_effect=%-4s",
                             str(field.bit_offset()), str(field.number_of_fields()), field.side_effect())

        logger.debug("  parameters:")
        for param_key, param_val in self._parameters.items():
            if isinstance(param_val, dict):
                for _key, _val in param_val.items():
                    logger.debug("      %-25s  %s", "{}.{}".format(param_key, _key), str(_val))
            else:
                logger.debug("      %-20s  %s", param_key, str(param_val))


class PeripheralLibrary(object):
    """ List of all information for peripheral config files in the root dir
    """
    def __init__(self, root_dir=None, file_extension='.peripheral.yaml'):
        """Store the dictionaries from all file_name files in root_dir."""
        self.root_dir   = root_dir
        logger.debug("****PERIPHERAL LIBRARY FUNCTION CALLED*****\n")

        # all peripheral files have the same double file extension ".peripheral.yaml"
        self.file_extension  = file_extension
        self.library = collections.OrderedDict()
        self.nof_peripherals = 0  # number of peripherals

        exclude = set([path_string(os.path.expandvars('$ARGS_BUILD_DIR'))])
        if root_dir is not None:
            for root, dirs, files in os.walk(self.root_dir, topdown=True):  # topdown=False):
                if 'tools' in root:
                    # skip tools dir
                    continue
                dirs[:] = [d for d in dirs if path_string(root + d) not in exclude and '.svn' not in d]
                for name in files:
                    if self.file_extension in name and name[0] != '.':
                        try :
                            library_config = yaml.full_load(open(os.path.join(root, name), 'r'))
                        except yaml.YAMLError as exc:
                            logger.error('Failed parsing YAML file "{}". Check file for YAML syntax errors'.format(name))
                            if hasattr(exc, 'problem_mark'):
                                mark = exc.problem_mark
                                logger.error("Error position: (%s:%s)" % (mark.line + 1, mark.column + 1))
                            raise ARGSYamlError
                            continue

                        if not isinstance(library_config, dict):
                            logger.warning('File %s is not readable as a dictionary, it will not be'
                                           ' included in the RadioHDL library of ARGS peripherals',
                                           name)
                            continue
                        else:
                            try:
                                library_config['schema_type']
                                library_config['peripherals']
                                library_config['hdl_library_name']
                            except KeyError:
                                logger.warning('File %s will not be included in the RadioHDL library.'
                                               '%s is missing schema_type and/or peripherals and/or hdl_library_name key',
                                               name, name)
                                continue
                        lib_name = library_config['hdl_library_name']  # name.replace(file_extension, '')
                        if lib_name != name.split('.')[0]:
                            logger.error("File %s has mismatching filename '%s' and hdl_library_name '%s'",
                                         os.path.join(root, name), name.split('.')[0], lib_name)
                            sys.exit()
                        if self.library.get(lib_name, None) is None:  # check peripheral library name is unique
                            self.library.update({lib_name: {'file_path': root,
                                                            'file_path_name': os.path.join(root, name),
                                                            'peripherals': collections.OrderedDict(),
                                                            'description': library_config.get('hdl_library_description', "")}})
                        else :
                            logger.error("More than one instance of args peripheral library '%s' found under %s", lib_name, root_dir)
                            logger.error("\nConflicting files:\n\t%s\n\t%s", self.library[lib_name]['file_path_name'], os.path.join(root, name))
                            sys.exit()

    def get_file_path_name(self, lib_name):
        """
        return peripheral library file_path_name if found else None
        """
        try:
            return self.library[lib_name]['file_path_name']
        except KeyError:
            logger.error("peripheral library %s not found.", lib_name)
        raise ARGSNameError

    def read_peripheral_files(self, file_path_names=None):
        """Read the peripheral information from all peripheral files that were found in the root_dir tree."""
        self.peripherals = {}
        if file_path_names is None:
            file_path_names = [lib_dict['file_path_name'] for lib_dict in self.library.values()]
        elif not isinstance(file_path_names, list):
            file_path_names = [file_path_names]

        for fpn in file_path_names:
            logger.debug("Load peripheral(s) from '%s'", fpn)
            library_config = yaml.full_load(open(fpn, 'r'))
            for peripheral_config in library_config['peripherals']:
                try :
                    peripheral = deepcopy(Peripheral(peripheral_config))
                except ARGSNameError:
                    logger.error("Invalid peripheral_name '%s' in *.peripheral.yaml",
                                 peripheral_config['peripheral_name'], library_config['hdl_library_name'])
                    sys.exit()
                logger.debug("  read peripheral '%s'" % peripheral.component_name())
                self.library[library_config['hdl_library_name']]['peripherals'].update({peripheral.component_name(): peripheral})
                self.nof_peripherals = self.nof_peripherals + 1
        # self.nof_peripherals = len(self.peripherals)  # number of peripherals
        return

    def find_peripheral(self, periph_name, peripheral_library=None, fpga_library=None):
        """ find peripheral referenced by <lib_name>/<periph_name> or just <periph_name> where possible

        component_name = peripheral_config['peripheral_name'] if '/' not in peripheral_config['peripheral_name'] else peripheral_config['peripheral_name'].split('/')[1]
        component_lib  = None if '/' not in peripheral_config['peripheral_name'] else peripheral_config['peripheral_name'].split('/')[0]
        peripheral_from_lib = self.peri_lib.find_peripheral(component_name, component_lib, self.fpga_name)

        """
        if peripheral_library is not None:
            #print('{}'.format(peripheral_library in self.library) )
            #print('{}'.format(periph_name in self.library[peripheral_library]['peripherals']) )
            #print('{}'.format(self.library[peripheral_library]) )
            if peripheral_library in self.library and periph_name in self.library[peripheral_library]['peripherals']:
                logger.debug("Peripheral %s found under library %s", periph_name, peripheral_library)
                return self.library[peripheral_library]['peripherals'][periph_name]
            else:
                try:
                    file_path_name = self.get_file_path_name(peripheral_library)
                    self.read_peripheral_files(file_path_name)
                    return self.library[peripheral_library]['peripherals'][periph_name]
                except:
                    logger.error("No peripheral library '%s' found under %s", peripheral_library, self.root_dir)
                    sys.exit()
                return None
        else :
            matching_libs = []
            # try to find unique instance of peripheral, failing that look for local
            for lib in self.library:
                if periph_name in self.library[lib]['peripherals']:
                    matching_libs.append(lib)

            matches = len(matching_libs)
            if matches > 1:
                if fpga_library is not None and fpga_library in matching_libs:
                    logger.debug("Multiple peripherals named %s found under libs %s, limiting peripheral search to local design library",
                                 periph_name, ' '.join(matching_libs).upper(), fpga_library)
                    return self.library[fpga_library]['peripherals'].get(periph_name, None)
                else:
                    print(' '.join(matching_libs))
                    logger.error("Multiple peripherals named '%s' found under libs %s, please specify peripheral library for peripheral %s in format '<lib_name>/%s'",
                                 periph_name, ' '.join(matching_libs).upper(), periph_name, periph_name)
                    sys.exit()
            elif matches == 1:
                logger.debug("Peripheral %s found under library %s",
                             periph_name, matching_libs[0])
                return self.library[matching_libs[0]]['peripherals'][periph_name]
            else:
                logger.error("No matching peripherals for '%s' found under %s",
                             periph_name, self.root_dir)
                sys.exit()
        return None

    def show_overview(self, header=True):
        """ print system overview
        """
        if header:
            logger.debug("---------------------")
            logger.debug("PERIPHERALS OVERVIEW:")
            logger.debug("---------------------")

        for peripheral in self.peripherals.values():
            peripheral.show_overview(header=False)
