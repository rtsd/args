# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author  Date
#   HJ    jan 2017  Original
#   EK    feb 2017
#   PD    feb 2017
#   PD    nov 2019
#
###############################################################################

import os
import sys
from copy import deepcopy
import logging
import yaml
import time
import collections
from py_args_lib import *
from peripheral_lib import *
import numpy as np

logger = logging.getLogger('main.fpga')


class FPGA(object):
    """ An FPGA consist of a set of one or more Peripherals.
    """
    def __init__(self, file_path_name=None, periph_lib=None, use_avalon_base_addr=None):
        self.file_path_name  = file_path_name
        self.root_dir        = os.environ['ARGS_WORK']
        if periph_lib is None:
            self.peri_lib = PeripheralLibrary(self.root_dir)
        else:
            self.peri_lib = periph_lib

        self.use_avalon_base_addr = False if use_avalon_base_addr is None else use_avalon_base_addr

        self.hdl_library_name = ""
        self.fpga_name        = ""
        self.fpga_description = ""
        self.parameters = {}
        # self.peripherals = {}
        self.peripherals = collections.OrderedDict()
        self.locked_base_addresses = {}
        self.valid_file_type = False
        self.nof_lite = 0
        self.nof_full = 0
        self.address_map = collections.OrderedDict()
        logger.debug("***FPGA object instantiation: creating for %s", file_path_name)

        # EK FIXME: a missing required key does not give a clear error message (e.g. missing mm_port_names)

        # EK FIXME: need to specify board_name explicitely (via e.g. board_name cmd line argument, or buildset_name like with RadioHDL) instead of derive from a dir name.
        if 'boards' in self.file_path_name:
            fpn_split = self.file_path_name.split('/')
            self.board_name = fpn_split[fpn_split.index('boards') + 1]
        elif 'unb2b' in self.file_path_name:
            self.board_name = 'uniboard2b'
        elif 'unb2c' in self.file_path_name:
            self.board_name = 'uniboard2c'
        else:
            self.board_name = None
            logger.error("No boardname found in path")

        if file_path_name is None:
            logger.debug("No FPGA configuration file specified")
            self.fpga_config = None
        else:
            # list of peripheral configurations that are read from the available peripheral files
            self.fpga_config = self.read_fpga_file(file_path_name)
            #self.read_used_peripherals()
            self.create_fpga()

    def is_valid(self):
        """ return False or True if the given file is a valid FPGA file """
        return self.valid_file_type

    def read_fpga_file(self, file_path_name):
        """Read the FPGA information from the file_path_name file."""

        logger.debug("Load FPGA from '%s'", file_path_name)
        fpga_config = yaml.full_load(open(file_path_name, 'r'))

        self.valid_file_type = True
        return fpga_config

    def create_fpga(self):
        """ Create a FPGA object based on the information in the fpga_config """
        logger.debug("Creating FPGA")
        logger.debug("Instantiating the peripherals from the peripheral Library")
        # self.fpga_name = self.fpga_config['hdl_library_name']
        self.hdl_library_name = self.fpga_config['hdl_library_name']
        self.fpga_name = self.fpga_config['fpga_name']

        if "fpga_description" in self.fpga_config:
            self.fpga_description = deepcopy(self.fpga_config['fpga_description'])

        if "parameters" in self.fpga_config:
            _parameters = deepcopy(self.fpga_config['parameters'])

            # keys with '.' in the name indicate the use of a structs
            # inside this class it is a dict in a dict
            for parameter_set in _parameters:
                name  = parameter_set['name']
                value = parameter_set['value']
                if '.' in name:
                    _struct, _key = name.split('.')
                    if _struct not in self.parameters:
                        self.parameters[_struct] = {}

            remove_list = []
            for parameter_set_nr, parameter_set in enumerate(_parameters):
                name  = parameter_set['name']
                value = parameter_set['value']
                if '.' in name:  # struct notation with dot
                    _struct, _name = name.split('.')
                    self.parameters[_struct][_name] = self._eval(str(value))
                else:
                    self.parameters[name] = self._eval(str(value))
                remove_list.append((parameter_set_nr, name))

            for parameter_set_nr, name in sorted(remove_list, reverse=True):
                logger.debug("delete '%s' from parameters", name)
                del _parameters[parameter_set_nr]

            for parameter_set in _parameters:
                name  = parameter_set['name']
                value = parameter_set['value']
                logger.debug("eval of name=%s and value=%s not posible", name, value)

            logger.debug("parameters=%s", str(self.parameters))

        for peripheral_config in self.fpga_config['peripherals']:
            # (Deep)Copy the peripheral from the library in order to avoid creating a reference
            component_name         = peripheral_config['peripheral_name'] if '/' not in peripheral_config['peripheral_name'] else peripheral_config['peripheral_name'].split('/')[1]
            component_lib          = None if '/' not in peripheral_config['peripheral_name'] else peripheral_config['peripheral_name'].split('/')[0]
            component_prefix       = peripheral_config.get('peripheral_group', None)
            number_of_peripherals  = int(self._eval(peripheral_config.get('number_of_peripherals', 1)))

            peripheral_from_lib = deepcopy(self.peri_lib.find_peripheral(component_name, component_lib, self.fpga_name))
            if peripheral_from_lib is None:
                logger.error("Peripheral component '%s' referenced in %s.fpga.yaml not found in peripheral library '%s'",
                             component_name, self.fpga_name, component_lib if component_lib is not None else '')
                sys.exit()

            logger.debug(" Finding %s", peripheral_from_lib.name())

            if 'parameter_overrides' in peripheral_config:
                logger.debug("parameters=%s", str(peripheral_config['parameter_overrides']))
                for parameter_set in peripheral_config['parameter_overrides']:
                    name  = parameter_set['name']
                    value = parameter_set['value']
                    peripheral_from_lib.parameter(key=name, val=self._eval(value))

            if 'mm_port_names' in peripheral_config:
                logger.debug("mm_port_names=%s", peripheral_config['mm_port_names'])
                for mm_port_nr, mm_port_name in enumerate(peripheral_config['mm_port_names']):
                    peripheral_from_lib.set_user_defined_mm_port_name(mm_port_nr, mm_port_name)

            if 'lock_base_address' in peripheral_config:
                logger.debug("lock_base_address=%s", str(peripheral_config['lock_base_address']))
                self.locked_base_addresses[peripheral_from_lib.name()] = peripheral_config['lock_base_address']

            if 'peripheral_span' in peripheral_config:
                logger.debug("peripheral_span=%s", str(peripheral_config['peripheral_span']))
                peripheral_from_lib.peripheral_span(self._eval(peripheral_config['peripheral_span']))

            peripheral_from_lib.number_of_peripherals(number_of_peripherals)
            peripheral_from_lib.prefix(component_prefix)
            peripheral_name = []
            if component_prefix not in (None, ''):
                peripheral_name.append(component_prefix)
            peripheral_name.append(component_name)
            peripheral_from_lib.name('_'.join(peripheral_name))

            if peripheral_from_lib.name() not in self.peripherals:
                self.peripherals[peripheral_from_lib.name()] = deepcopy(peripheral_from_lib)
            else:
                logger.error("  Duplicate found: use unique labels per instance in %s.fpga.yaml to distinguish "
                             "between multiple instances of the same peripheral.\n"
                             "  Cannot add a second instance of peripheral: %s",
                             self.fpga_name, peripheral_from_lib.name())
                sys.exit()

        logger.debug("Start evaluating the peripherals")
        for peripheral_config in self.peripherals.values():
            peripheral_config.eval_peripheral()

        if self.use_avalon_base_addr is True:
            logger.debug("Use qsys/sopc base addresses for the mm_ports")
            if self.read_avalon_address_map() == -1:
                logger.debug("Now trying auto_create")
                self.create_address_map()
        else:
            self.create_address_map()
        return

    def _eval(self, val):
        """ evaluate val.
        1) trying to parse values of known parameter into the value
        2) eval the value, known imported function are also evaluated """

        _val = str(val)
        # first replace all knowns parameter names with its assigned value
        for key1, val1 in iter(self.parameters.items()):
            # if val is a dict, in vhdl it's a struct
            if isinstance(val1, dict):
                for key2, val2 in iter(val1.items()):
                    key = "{}.{}".format(key1, key2)
                    # logger.debug("replace %s with %s", key, str(val2))
                    _val = _val.replace(key, str(val2))
            else:
                # logger.debug("replace %s with %s", key1, str(val1))
                _val = _val.replace(key1, str(val1))
        result = eval(_val)
        logger.debug("_eval(%s) returns eval(%s) = %s", str(val), _val, str(result))
        return result

    def read_avalon_address_map(self):
        """
        read qsys file, and use base_addresses
        """
        filename = os.path.basename(self.file_path_name)
        dirname  = os.path.dirname(self.file_path_name)

        # Debug: cat log/gen_rom_mmap.log |grep 'Avalon'
        if '_sopc' in self.fpga_name:
            filename = 'sopc_{}.sopc'.format(self.fpga_name.replace('_sopc', ''))
            logger.debug('Avalon SOPC config filename = "%s"', filename)
        else:
            filename = 'qsys_{}.qsys'.format(self.fpga_name.replace('_qsys', ''))
            logger.debug('Avalon QSYS config filename = "%s"', filename)

        try:
            file_path_name = os.path.join(dirname, 'quartus', filename)
            os.stat(file_path_name)
        except FileNotFoundError:
            logger.debug('Avalon "%s" does not exist.', file_path_name)
            file_path_name = None

        # Avalon system should only be in 'mother' design, not in revision, so do not look for it in revision
        #EK: TODO: Remove commented code
        #if file_path_name is None:  # must be a revision, try 2 dirs up.
        #    try:
        #        file_path_name = os.path.join(dirname, '../..', 'quartus', filename)
        #        os.stat(file_path_name)
        #    except FileNotFoundError:
        #        logger.debug('Avalon "%s" does not exist.', file_path_name)
        #        file_path_name = None

        if file_path_name is None:
            logger.error('Avalon file "%s" not found', filename)
            return -1

        with open(file_path_name, 'r') as fd:
            logger.debug('Avalon read file "%s".', file_path_name)
            data = fd.read()

        avalon_base_addr = {}
        avalon_port_span = {}

        p1 = 0
        p1 = data.find('<key>data_master</key>', p1)
        p1 = data.find('address-map', p1)
        p1 = data.find('slave', p1)
        p2 = data.find('/address-map', p1)
        addr_data = data[p1:p2]
        addr_data = addr_data.replace('/&gt;&lt','')
        addr_data = addr_data.replace('slave name', 'mm_port_name')
        addr_data = addr_data.replace("'", '')
        addr_data = addr_data.split(';')
        #print(addr_data)
        perips_info = {}
        for ad in addr_data:
            if len(ad) == 0:
                continue
            perip_info = {}
            for ai in ad.split():
                k, v = ai.split('=')
                for v_end in ['.mem', '.s1', '.mms']:
                    if v_end in v:
                        v = v.replace(v_end, '')
                perip_info[k] = v
            perips_info[perip_info['mm_port_name']] = perip_info

            byte_address = int(perip_info['start'][2:], 16)
            byte_span = int(perip_info['end'][2:], 16) - byte_address
            avalon_base_addr[perip_info['mm_port_name'].upper()] = byte_address
            avalon_port_span[perip_info['mm_port_name'].upper()] = byte_span
        logger.debug('Avalon base adresses "%s"', avalon_base_addr)
        logger.debug('Avalon port spans "%s"', avalon_port_span)  # these are spans of ceil_pow2(number_of_peripherals) * ceil_pow2(number_of_mm_ports) * mm_port_span

        for peripheral in self.peripherals.values():
            #logger.debug('** PERIPHERAL: %21s base_addr=0x%08x [occupied size=0x%04x]',
            #             peripheral.name()[:20], lowest_free_addr, peripheral.reg_len)  # TODO

            for periph_num in range(peripheral.number_of_peripherals()):
                for mmport in peripheral.mm_ports:

                    base_addr = -1
                    if mmport.user_defined_name():
                        try:
                            base_addr = avalon_base_addr[mmport.user_defined_name()]
                            mmport.base_address(base_addr)
                        except:
                            logger.warn("Avalon base address for username '%s' not found", mmport.user_defined_name())

                    addr_map_name = [peripheral.name(), mmport.name()]

                    if isinstance(mmport, Register):
                        addr_map_name.append('reg')

                        # peripherals.peripheral_span() read from yaml file.
                        size_in_bytes = mmport.address_length() * ceil_pow2(mmport.number_of_mm_ports())
                        peripheral_span = int(max(size_in_bytes, peripheral.peripheral_span()))
                        logger.debug("Avalon REG MM port %s_%s has span 0x%x", peripheral.name(), mmport.name() , peripheral_span)

                        if peripheral.number_of_peripherals() > 1:
                            addr_map_name.append(str(periph_num))
                        mm_port_name = '_'.join(addr_map_name)

                        self.address_map[mm_port_name] = {'base': base_addr, 'span': peripheral_span, 'type': 'LITE',
                                                          'port_index': self.nof_lite, 'peripheral': peripheral,
                                                          'periph_num': periph_num, 'mm_port': mmport}
                        logger.debug("Avalon Register for %s has span 0x%x", peripheral.name(), peripheral_span)

                    elif isinstance(mmport, RAM):
                        addr_map_name.append('ram')

                        size_in_bytes = mmport.address_length() * ceil_pow2(mmport.number_of_mm_ports())
                        peripheral_span = int(max(size_in_bytes, peripheral.peripheral_span()))

                        logger.debug("Avalon RAM MM port %s_%s has span 0x%x", peripheral.name(), mmport.name() , peripheral_span)

                        if peripheral.number_of_peripherals() > 1:
                            addr_map_name.append(str(periph_num))
                        mm_port_name = '_'.join(addr_map_name)

                        self.address_map[mm_port_name] = {'base': base_addr, 'span': peripheral_span, 'type': 'FULL',
                                                          'port_index': self.nof_full, 'peripheral': peripheral,
                                                          'periph_num': periph_num, 'mm_port': mmport}

                    elif isinstance(mmport, FIFO):
                        addr_map_name.append('fifo')

                        size_in_bytes = mmport.address_length() * ceil_pow2(mmport.number_of_mm_ports())
                        peripheral_span = int(max(size_in_bytes, peripheral.peripheral_span()))

                        logger.debug("Avalon FIFO MM port %s_%s has span 0x%x", peripheral.name(), mmport.name() , peripheral_span)

                        if peripheral.number_of_peripherals() > 1:
                            addr_map_name.append(str(periph_num))
                        mm_port_name = '_'.join(addr_map_name)

                        self.address_map[mm_port_name] = {'base': base_addr, 'span': peripheral_span, 'type': 'FULL',
                                                          'port_index': self.nof_full, 'access': mmport.access_mode(),
                                                          'peripheral': peripheral, 'periph_num': periph_num, 'mm_port': mmport}
        return 0

    def create_address_map(self):
        """ Preserves order of entry from fpga.yaml
            Based on vivado limitations, minimum span is 4kB
            Configurable ranges are 4k, 8k, 16k, 32k, 64k i.e. 2^(12,13,14,15,16)
            There is a maximum of one register group per peripheral
        """
        logger.debug("create_address_map('%s')", self.fpga_name)

        # Largest peripheral will determine spacing between peripheral base addresses
        largest_addr_range = 4096  # minimal allowed address-decode spacing with Xilinx interconnect
        for peripheral in self.peripherals.values():
            if peripheral.reg_len > largest_addr_range:
                largest_addr_range = peripheral.reg_len
        peripheral_spacing = ceil_pow2(largest_addr_range)

        lowest_free_addr = 0
        for peripheral in self.peripherals.values():
            # Ensure peripheral base is aligned to address decode
            lowest_free_addr = int(np.ceil(lowest_free_addr / peripheral_spacing) * peripheral_spacing)

            logger.debug('** PERIPHERAL: %21s base_addr=0x%08x [occupied size=0x%04x]',
                         peripheral.name()[:20], lowest_free_addr, peripheral.reg_len)

            # assigned_reg = False
            # _nof_regs =  sum([isinstance(mmport, Register) for mmport in peripheral.mm_ports])
            # _minus_regs = _nof_regs - 1 if _nof_regs > 0 else 0
            # _nof_mm_ports = len(peripheral.mm_ports) - _minus_regs
            for periph_num in range(peripheral.number_of_peripherals()):
                assigned_reg = False
                for mmport in peripheral.mm_ports:
                    if isinstance(mmport, Register):
                        mm_port_type = 'reg'
                        if assigned_reg is False:  # calc for entire register MM port
                            reg_span = ceil_pow2(max(peripheral.reg_len, 4096))
                            lowest_free_addr = int(np.ceil(lowest_free_addr/reg_span)*reg_span)
                            register_base = lowest_free_addr
                        else :
                            self.nof_lite = self.nof_lite - 1
                            lowest_free_addr = register_base + (mmport.base_address() if not any(mmport.rams) else mmport.rams[0].base_address())
                        ram_span = mmport.base_address() - mmport.rams[0].base_address() if any(mmport.rams) else 0
                        mm_port_span = mmport.address_length() * mmport.number_of_mm_ports() + ram_span

                        _name_list = [peripheral.name(), mmport.name(), mm_port_type]
                        if peripheral.number_of_peripherals() > 1:
                            _name_list.append(str(periph_num))
                        mm_port_name = '_'.join(_name_list)

                        self.address_map[mm_port_name] = {'base': lowest_free_addr, 'span': mm_port_span, 'type': 'LITE',
                                                          'port_index': self.nof_lite, 'peripheral': peripheral,
                                                          'periph_num': periph_num, 'mm_port': mmport}
                        logger.debug("Register for %s has span 0x%x", peripheral.name(), mm_port_span)
                        lowest_free_addr = lowest_free_addr + int(mm_port_span)
                        self.nof_lite = self.nof_lite + 1
                        assigned_reg = True

                    elif isinstance(mmport, RAM):
                        mm_port_type = 'ram'
                        size_in_bytes = mmport.address_length() * ceil_pow2(mmport.number_of_mm_ports())
                        mm_port_span = ceil_pow2(max(size_in_bytes, 4096))
                        logger.debug("MM port %s_%s has span 0x%x", peripheral.name(), mmport.name() , mm_port_span)
                        # mm_port_name = mmport.name() + ('_{}'.format(mm_port_no) if mmport.number_of_mm_ports() >1 else '')
                        lowest_free_addr = int(np.ceil(lowest_free_addr / mm_port_span) * mm_port_span)

                        _name_list = [peripheral.name(), mmport.name(), mm_port_type]
                        if peripheral.number_of_peripherals() > 1:
                            _name_list.append(str(periph_num))
                        mm_port_name = '_'.join(_name_list)

                        self.address_map[mm_port_name] = {'base': lowest_free_addr, 'span': mm_port_span, 'type': 'FULL',
                                                          'port_index': self.nof_full, 'peripheral': peripheral,
                                                          'periph_num': periph_num, 'mm_port': mmport}
                        self.nof_full = self.nof_full + 1
                        lowest_free_addr = lowest_free_addr + mm_port_span

                    elif isinstance(mmport, FIFO):
                        mm_port_type = 'fifo'
                        size_in_bytes = mmport.address_length()
                        mm_port_span = ceil_pow2(max(size_in_bytes, 4096))
                        for i in range(mmport.number_of_mm_ports()):
                            lowest_free_addr = int(np.ceil(lowest_free_addr / mm_port_span) * mm_port_span)

                            _name_list = [peripheral.name(), mmport.name(), mm_port_type]
                            if peripheral.number_of_peripherals() > 1:
                                _name_list.append(str(periph_num))
                            mm_port_name = '_'.join(_name_list)

                            self.address_map[mm_port_name] = {'base': lowest_free_addr, 'span': mm_port_span, 'type': 'FULL',
                                                              'port_index': self.nof_full, 'access': mmport.access_mode(),
                                                              'peripheral': peripheral, 'periph_num': periph_num, 'mm_port': mmport}
                            self.nof_full = self.nof_full + 1
                            lowest_free_addr = lowest_free_addr + mm_port_span


    def show_overview(self):
        """ print FPGA overview
        """
        logger.debug("----------------------")
        logger.debug(" FPGA OVERVIEW ")
        logger.debug("----------------------")
        logger.debug("FPGA name '%s'", self.fpga_name)
        logger.debug("- FPGA parameters:")
        for key, val in iter(self.parameters.items()):
            if isinstance(val, dict):
                for _key, _val in val.items():
                    logger.debug("  %-25s  %s", "{}.{}".format(key, _key), str(_val))
            else:
                logger.debug("  %-20s  %s", key, str(val))
        logger.debug("- FPGA Address Map:")
        for mm_port_name, attributes in self.address_map.items():
            logger.debug("  %-30s 0x%x, range %5.3fkB at port MSTR_%s[%d]",
                         mm_port_name, attributes['base'], float(attributes['span'] / 1024.0), attributes['type'], attributes['port_index'])
        logger.debug("- Peripherals:")
        for peripheral in sorted(self.peripherals):
            self.peripherals[peripheral].show_overview(header=False)


class FPGALibrary(object):
    """
    List of all information for FPGA config files in the root dir, intended for use in hdl_config.py
    """

    def __init__(self, root_dir=None, use_avalon_base_addr=None):
        self.root_dir = root_dir
        self.use_avalon_base_addr = use_avalon_base_addr
        self.file_extension = ".fpga.yaml"
        self.library = {}
        self.nof_peripherals = 0
        tic = time.time()
        if root_dir is not None:
            for root, dirs, files in os.walk(self.root_dir, topdown=True):
                if 'tools' in root:
                    continue  # skip tools dir
                for name in files:
                    if not name.endswith(self.file_extension):
                        continue  # skip, no matching file extension
                    try :
                        # try to read yaml file
                        library_config = yaml.full_load(open(os.path.join(root, name), 'r'))
                    except yaml.YAMLError as exc:
                        logger.error('Failed parsing YAML file "%s". Check file for YAML syntax errors', name)
                        if hasattr(exc, 'problem_mark'):
                            mark = exc.problem_mark
                            logger.error("Error position: (%s:%s)", mark.line + 1, mark.column + 1)
                        #logger.error('ERROR:\n' + sys.exc_info()[1])
                        raise ARGSYamlError
                        continue


                    if not isinstance(library_config, dict):
                        logger.warning('File %s is not readable as a dictionary, it will not be'
                                       ' included in the FPGA library of peripherals', name)
                        continue
                    lib_name = name.replace(self.file_extension, '')
                    logger.debug("Found fpga.yaml file %s", lib_name)
                    if lib_name in self.library:
                        logger.warning("%s library already exists in FPGALibrary, being overwritten", lib_name)
                    # looks a valid library
                    self.library[lib_name] = {'file_path': root, 'file_path_name': os.path.join(root, name), 'peripherals': {}}
        toc = time.time()
        logger.debug("FPGALibrary os.walk took %.4f seconds", (toc - tic))
        self.peripheral_lib = PeripheralLibrary(self.root_dir)
        #self.read_fpga_files()

    def read_fpga_files(self, file_path_names=None):
        """
           Read the information from all FPGA files that were found in the root_dir tree
        """

        if file_path_names is None:
            file_path_names = [lib_dict['file_path_name'] for lib_dict in self.library.values()]
        elif not isinstance(file_path_names, list):
            file_path_names = [file_path_names]

        for fpn in file_path_names:
            logger.debug("Creating ARGS FPGA object from %s", fpn)
            tic = time.time()
            fpga = FPGA(fpn, periph_lib=self.peripheral_lib, use_avalon_base_addr=self.use_avalon_base_addr)
            toc = time.time()
            logger.debug("fpga creation for %s took %.4f seconds", fpn, (toc - tic))
            fpga.show_overview()
            self.library[fpga.fpga_name].update({'fpga': fpga})
            for lib, peripheral in fpga.peripherals.items():
                self.library[fpga.fpga_name]['peripherals'].update({lib: peripheral})

    def get_file_path_name(self, fpga_name):
        """
        return fpga file_path_name if found else None
        """
        try:
            return self.library[fpga_name]['file_path_name']
        except KeyError:
            logger.error("fpga %s not found.", fpga_name)
        raise ARGSNameError

    def get_fpga(self, fpga_name):
        """
        return fpga system information if found else None
        """
        try:
            if 'fpga' not in self.library[fpga_name]:
                logger.debug('fpga not loaded, load it now')
                file_path_name = self.get_file_path_name(fpga_name)
                self.read_fpga_files(file_path_name)
            return self.library[fpga_name]['fpga']
        except KeyError:
            logger.error("fpga %s not found.", fpga_name)
        raise ARGSNameError
