# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author  Date
#   HJ    jan 2017  Original
#   EK    feb 2017
#   PD    feb 2017
#   PD    sep 2020 cleanup code
#
###############################################################################

from math import ceil
import logging
from constants import *
from common_func import ceil_pow2, ceil_log2
from base_object import BaseObject
from field import Field

logger = logging.getLogger('main.perip.ram')


MIN_BRAM_DEPTH = 128   # EK TODO:remove this limitation

class RAM(Field):
    """ A RAM is a Field that is repeated address_length times
    """
    def __init__(self, name, settings=None):
        super().__init__(name, settings)
        self.set_kv('type', 'RAM')
        self.name(name)
        self.description = ""

        self._valid_keys = ['number_of_mm_ports', 'mm_width', 'user_width']
        
        self._args.update({'number_of_mm_ports' : DEFAULT_NUMBER_OF_MM_PORTS,
                           'mm_port_span'       : DEFAULT_MM_PORT_SPAN})
        self._address_length = MM_BUS_SIZE

    def number_of_mm_ports(self, val=None):
        """ set/get number_of_mm_ports """
        if val is not None:
            self.set_kv('number_of_mm_ports', val)
            return
        return self._as_int('number_of_mm_ports')

    def mm_port_span(self, val=None):
        """ set/get mm_port_span """
        if val is not None:
            self.set_kv('mm_port_span', val)
            return
        return self._as_int('mm_port_span')

    def update_address_length(self):
        """ update total address_length of Register in bytes """
        n_fields = ceil_pow2(self.number_of_fields())
        self._address_length = max(n_fields * MM_BUS_SIZE, self.mm_port_span())
        # EK TODO: always use evaluated mm_port_span() and give error when it is not large enough, even better: do not calculate span at all so remove update_address_length().

    def address_length(self, val=None):
        """ set/get address_length of register in bytes
        val: if not None set address_length of register
        return: address_length of register """
        if val is not None:
            self._address_length = int(val)
            return
        return self._address_length
