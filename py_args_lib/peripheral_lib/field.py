# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author  Date
#   HJ    jan 2017  Original
#   EK    feb 2017
#   PD    feb 2017, nov 2019
#
###############################################################################

import logging
import sys
from constants import *
from common_func import ceil_pow2, ceil_log2
from base_object import BaseObject
from numpy import mod

logger = logging.getLogger('main.periph.field')


class Field(BaseObject):
    """ A field defines data at certain address or an array of addresses
    """
    def __init__(self, name, settings=None):
        super().__init__()

        self.name(name)

        self._valid_dict = {'field_name'        : {},
                            'field_description' : {},
                            'mm_width'          : {'max': 32, 'min': 1},       # EK: TODO:can this be larger e.g. 64 or 256 or don't care {}?
                            'bit_offset'        : {'max': UNASSIGNED_BIT, 'min': 0},
                            'user_width'        : {'max': 2048, 'min': 1},
                            'user_word_order'   : {},
                            'access_mode'       : {},
                            'side_effect'       : {},
                            'address_offset'    : {'max': 262144, 'min': 0, 'word_aligned': True},
                            'number_of_fields'  : {'max': 262144, 'min': 1},
                            'reset_value'       : {'max': 131071},
                            'radix'             : {},
                            'resolution_w'      : {}}

        self._args.update({'field_description': DEFAULT_FIELD_DESCRIPTION,
                           'mm_width'         : DEFAULT_FIELD_MM_WIDTH,
                           'user_word_order'  : DEFAULT_FIELD_USER_WORD_ORDER,
                           'bit_offset'       : DEFAULT_FIELD_BIT_OFFSET,
                           'access_mode'      : DEFAULT_FIELD_ACCESS_MODE,
                           'side_effect'      : DEFAULT_FIELD_SIDE_EFFECT,
                           'address_offset'   : DEFAULT_FIELD_ADDRESS_OFFSET,
                           'number_of_fields' : DEFAULT_FIELD_NUMBER_OF_FIELDS,
                           'reset_value'      : DEFAULT_FIELD_RESET_VALUE,
                           'radix'            : DEFAULT_FIELD_RADIX,
                           'resolution_w'     : DEFAULT_FIELD_RESOLUTION_W,
                           'group_name'       : None})

        if settings is not None:
            # print(name)
            # logger.debug(f"settings=%s", str(settings))
            for key, val in settings.items():
                if key in self._valid_dict:  # self._valid_keys:

                    if key == 'access_mode' and val.upper() not in VALID_FIELD_ACCESS_MODES:
                        logger.error("Field.__init__(), Not a valid acces_mode '%s'", val)
                        self.success = False
                    elif key == 'side_effect' and val.upper() not in VALID_FIELD_SIDE_EFFECTS:
                        logger.error("Field.__init__(), Not a valid side_effect '%s'", val)
                        self.success = False
                    else:
                        self.set_kv(key, val)
                else:
                    logger.error("Field.__init__(), Not a valid key '%s'", key)
                    self.success = False

    def __lt__(self, other):
        return self.address_offset() < other.address_offset()

    # EK TODO:why are all these field key methods slightly different, is there not a common structure?
    # EK TODO:check parameter passing to user_width.

    def number_of_fields(self, val=None):
        """ set/get number of fields """
        if val is not None:
            self.set_kv('number_of_fields', val)
            return
        return self._as_int('number_of_fields')

    def group_name(self, val=None):
        """ set/get group name """
        if val is not None:
            self.set_kv('group_name', val)
            return
        if self.get_kv('group_name') is None:
            return None
        return self._as_str('group_name')

    def mm_width(self, val=None):    # EK: TODO:method name seems to have to be the same as the key name, is that intended?
        """ set/get mm_width of field
        val: if not None set mm_width of field
        return: actual mm_width of field """
        if val is not None:
            return self.set_kv('mm_width', val)
        _val = self._as_int('mm_width')            # EK TODO:_val is not used
        if not self.is_valid('mm_width'):
            return None
        return self._as_int('mm_width')

    def bit_offset(self, val=None):
        """ set/get bit_offset of field
        val: if not None set bit_offset of field
        return: actual bit_offset of field """
        if val is not None:
            return self.set_kv('bit_offset', val)
        if not self.is_valid('bit_offset'):
            return None
        return self._as_int('bit_offset')

    def access_mode(self, val=None):
        """ set/get access_mode of field
        val: if not None and a valid mode set access_mode of field
        return: actual access_mode of field """
        if val is not None:
            if val.upper() in VALID_FIELD_ACCESS_MODES:
                return self.set_kv('access_mode', val.upper())
            else:
                logger.error("unknown access_mode '%s'", val)
                self.success = False
                return False
        return self._as_str('access_mode').upper()

    def side_effect(self, val=None):
        """ set/get side_effect of field
        val: if not None and a valid side_effect set side_effect of field
        return: actual side_effect of field """
        if val is not None:
            if val.upper() != 'NONE':
                vals = val.split(',')
                for _val in vals:
                    _val = _val.strip()
                    if _val.upper() not in VALID_FIELD_SIDE_EFFECTS:
                        logger.error("unknown side_effect '%s'", _val)
                        self.success = False
                        return False
                return self.set_kv('side_effect', val.upper())
        return self._as_str('side_effect').upper()

    def address_offset(self, val=None):
        """ set/get address offset of field in bytes
        val: if not None set address offset of field
        return: actual address offset of field """
        if val is not None:
            return self.set_kv('address_offset', val)
        if not self.is_valid('address_offset'):
            return None
        return self._as_int('address_offset')

    def reset_value(self, val=None):
        """ set/get default hardware reset value of field
        val: if not None set default value of field
        return: active hardware reset value of field """
        if val is not None:
            return self.set_kv('reset_value', val)
        if not self.is_valid('reset_value'):
            return None
        return self._as_int('reset_value')

    def radix(self, val=None):
        """ set/get radix value of field
        val: if not None set default value of field
        return: active radix value of field """
        if val is not None:
            #print(val.lower())
            if val.lower() in VALID_FIELD_RADIXS:
                return self.set_kv('radix', val.lower())
            else:
                logger.error("unknown radix '%s'", val)
                self.success = False
                return False
        return self._as_int('radix')

    def resolution_w(self, val=None):
        """ set/get resolution_w value of field
        val: if not None set default value of field
        return: active resolution_w value of field """
        if val is not None:
            return self.set_kv('resolution_w', val)
        if not self.is_valid('resolution_w'):
            return None
        return self._as_int('resolution_w')

    def field_description(self, val=None):
        """ set/get description of field
        val: if not None set description of field
        return: description of field """
        if val is not None:
            return self.set_kv('field_description', val)
        return self._as_str('field_description')

    #def user_width(self, val=None):
    #    """ set/get width of RAM MM bus side interface  """
    #    if val is not None:
    #        self.set_kv('user_width', val)
    #        return
    #    return self._as_int('user_width', default=32)

    def user_width(self, val=None):
        """ set/get user_width value of field
        val: if not None set default value of field
        return: active user_width value of field """
        if val is not None:
            return self.set_kv('user_width', val)
        if not self.is_valid('user_width'):
            return None
        return self._as_int('user_width')

    def user_word_order(self, val=None):
        """ set/get user_word_order """
        if val is not None:
            self.set_kv('user_word_order', val)
            return
        return self._as_int('user_word_order')

    def user_word_order(self, val=None):
        """ set/get user_word_order of field
        val: if not None and a valid user word order then set user_word_order of field
        return: actual user_word_order of field """
        if val is not None:
            if val.lower() in VALID_FIELD_USER_WORD_ORDERS:
                return self.set_kv('user_word_order', val.lower())
            else:
                logger.error("unknown user_word_order '%s'", val)
                self.success = False
                return False
        return self._as_str('user_word_order').lower()

    def base_address(self, val=None):
        """ set/get base_address in bytes """
        if val is not None:
            if mod(val, MM_BUS_SIZE):  # don't need check here if tool calcs are correct
                logger.error("Base address for field {} is not word aligned".format(field.name()))
            self.set_kv('base_addr', val)
            return
        return self._as_int('base_addr', default=0)

    def is_valid(self, yaml_key):
        """ Check value falls within min max range for keys that are ints """
        _min = self._valid_dict[yaml_key].get('min', None)
        _max = self._valid_dict[yaml_key].get('max', None)
        check_alignment = self._valid_dict[yaml_key].get('word_aligned', False)

        if yaml_key in ['reset_value']:
            _min = 0
            _max = int(1 << self._args.get('mm_width', 32)) - 1

        _val = self._as_int(yaml_key)
        if not isinstance(_val, int):
            return True  # may not have been evaluated from parameter yet, skip

        if check_alignment:
            if mod(_val, MM_BUS_SIZE):
                logger.error("Address offset for field %s is not word aligned (%s=%s)",
                             self.name(), yaml_key, str(_val))
                sys.exit()

        if _min is not None:
            if _val < _min:
                logger.error("Field '%s': Value %s for '%s' key is outside of supported range, min is %s. Value reset to None",
                             self.name(), str(_val), yaml_key, str(_min))
                return False
        if _max is not None:
            if _val > _max:
                logger.error("Field '%s': Value %s for '%s' key is outside of supported range, max is %s. Value reset to None",
                             self.name(), str(_val), yaml_key, str(_max))
                return False
        return True

    # TODO: calc size in bytes
    # def get_byte_size(self):

    #def address_length(self, val=None):
    #    """ set/get address length of field
    #    val: if not None set address length of field
    #    return: active address_length of field """
    #    if val is not None:
    #        return self.set_kv('address_length', val)
    #    return self._as_int('address_length')
