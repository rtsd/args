# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author  Date
#   HJ    jan 2017  Original
#   EK    feb 2017
#   PD    feb 2017
#
###############################################################################

from math import ceil
from numpy import mod
import logging
from constants import *
from common_func import ceil_pow2, ceil_log2
from base_object import BaseObject
from field import Field

logger = logging.getLogger('main.periph.register')


class Register(BaseObject):    # EK: TODO:rename to REG(), to match RAM and FIFO class names and to match mm_port_type key in peripheral.yaml
    """ A register consists of Fields
    """
    def __init__(self, name, fields=None, settings=None):
        super().__init__()

        self.set_kv('type', 'REG')
        self.name(name)

        self.fields = [] if fields is None else fields

        self.rams = []   # EK: TODO:remove rams, because no rams in Register

        self._valid_keys = ['number_of_mm_ports', 'address_length', 'mm_port_description', 'base_address']  # EK: TODO:why here and not in constants.py, best use only one place

        self._args.update({'number_of_mm_ports' : DEFAULT_NUMBER_OF_MM_PORTS,
                           'mm_port_span'       : DEFAULT_MM_PORT_SPAN,
                           'dual_clock'         : False,
                           'mm_port_description': DEFAULT_MM_PORT_DESCRIPTION})      # EK: TODO:what is the purpose of this list, it seems incomplete.
        self._address_length = MM_BUS_SIZE

    def number_of_mm_ports(self, val=None):
        """ set/get number_of_mm_ports """
        if val is not None:
            self.set_kv('number_of_mm_ports', val)
            return
        return self._as_int('number_of_mm_ports')

    def mm_port_span(self, val=None):
        """ set/get mm_port_span """
        if val is not None:
            self.set_kv('mm_port_span', val)
            return
        return self._as_int('mm_port_span')

    def add_field(self, name, settings):
        """ add new Field to Register with given settings
        """
        field = Field(name, settings)
        if field.success:
            # self.fields[name] = field
            self.fields.append(field)
            # self.update_address_length()
            return True
        return False

    def update_address_length(self):
        """ update total address_length of Register in bytes """
        if len(self.fields) == 0 and len(self.rams) == 0:
            self._address_length = 0
            return

        n_bytes = 0
        if any(self.fields):
            n_bytes += max(ceil_pow2(max([_field.address_offset() for _field in self.fields]) + MM_BUS_SIZE), self.mm_port_span())
            # EK TODO: always use evaluated mm_port_span() and give error when it is not large enough, even better: do not calculate span at all so remove update_address_length().
        
        if any(self.rams):
            #n_bytes += max(max([_field.address_offset() for _field in self.rams]) + MM_BUS_SIZE, self.mm_port_span if self.mm_port_span is not None else 0)
            n_bytes += ceil_pow2(self.rams[0].number_of_fields()) * MM_BUS_SIZE

        self._address_length = n_bytes

    def address_length(self, val=None):
        """ set/get address_length of register in bytes
        val: if not None set address_length of register
        return: address_length of register """
        if val is not None:
            if mod(val, MM_BUS_SIZE):  # dont need this if properly calcd
                logger.error("Invalid address length for register {}, not word aligned".format(self.name()))
                sys.exit()
            self._address_length = int(val)
            return
        return self._address_length

    def base_address(self, val=None):
        if val is not None:
            return self.set_kv('base_address', val)
        return self._as_int('base_address', default=0)
