# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################


import logging

logger = logging.getLogger('main.args_errors')


class ARGSNameError(Exception):
    pass
   

class ARGSModeError(Exception):
    pass


class ARGSYamlError(Exception):
    pass


def handle_args_error(error):
    # print(error)
    if error == ARGSModeError:
        logger.error("args mode error")
        return
    if error == ARGSNameError:
        logger.error("args name error, wrong fpga or peripheral name")
        return
    if error == ARGSYamlError:
        logger.error("args yaml error, wrong yaml file format")
        return
    
    logger.error("Unknown error %s", error)
    raise
