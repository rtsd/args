# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author  Date
#   PD    feb 2017, nov 2019
#
###############################################################################


"""
 Constants used by peripheral.py and fpga.py
 this constants can be used in the yaml files
"""

c_byte_w      = 8
c_word_w      = 32
c_nof_complex = 2

MM_BUS_WIDTH    = c_word_w                   # = 32 bits
MM_BUS_SIZE     = c_word_w // c_byte_w       # =  4 bytes

VALID_SCHEMA_NAME  = ['args']
VALID_SCHEMA_TYPES = ['fpga', 'peripheral']

VALID_MM_PORT_TYPES  = ['REG', 'RAM', 'FIFO', 'REG_IP']   # EK: TODO:Remove REG_IP

VALID_FIELD_ACCESS_MODES = ['RO', 'WO', 'RW']   # Read only, Write only, Read/Write. For FIFO use RO = read from FIFO, WO = write to FIFO.

VALID_FIELD_SIDE_EFFECTS = ['CLR', 'PR', 'PW', 'MON']   # EK: TODO:Remove MON

VALID_FIELD_USER_WORD_ORDERS = ['le', 'be']

# EK: Use same order of field keys as in ARGS document table (first name, then address/bits related keys, then other property keys)
VALID_FIELD_KEYS = ['field_description',
                    'number_of_fields',
                    'address_offset',
                    'bit_offset',
                    'mm_width',
                    'user_width',
                    'user_word_order',
                    'radix',
                    'resolution_w',
                    'access_mode',
                    'side_effect',
                    'reset_value']    # EK: TODO:field_name is missing in this list ? Use same order as in ARGS doc.
                                      # EK: TODO:how does this list relate to self._valid_dict() in fields.py, is this a duplicate check?

VALID_DEFAULT_FIELD_KEYS = ['field_description',
                            'number_of_fields',
                            'address_offset',
                            'mm_width',
                            'access_mode',
                            'side_effect',
                            'reset_value']   # EK: TODO:change to MANDATORY_FIELD_KEYS to have smaller list ? Current list is incomplete.

# EK: Use alphabetical order of radixs
VALID_FIELD_RADIXS = ['char8',
                      'cint16_ir',
                      'cint16_ri',
                      'cint32_ir',
                      'cint32_ri',
                      'cint64_ir',
                      'cint64_ri',
                      'int32',
                      'int64',
                      'uint32',
                      'uint64']  # EK: TODO:change complx into complex, but complex gets misinterpreted by Python --> no with complex_ri and comple_ir this is no longer an issue.


UNASSIGNED_ADDRESS = 16384     # EK: TODO:why is this needed ? Remove ?
UNASSIGNED_BIT = 32            # EK: TODO:why is this needed ? Remove ?

DEFAULT_NUMBER_OF_PERIPHERALS  = 1
DEFAULT_NUMBER_OF_MM_PORTS     = 1
DEFAULT_MM_PERIPHERAL_SPAN     = MM_BUS_SIZE
DEFAULT_MM_PORT_SPAN           = MM_BUS_SIZE
DEFAULT_MM_PORT_DESCRIPTION    = 'NONE'
DEFAULT_ADDRESS_LENGTH         = 1

DEFAULT_FIELD_MM_WIDTH         = MM_BUS_WIDTH
DEFAULT_FIELD_USER_WIDTH       = MM_BUS_WIDTH
DEFAULT_FIELD_USER_WORD_ORDER  = 'le'
DEFAULT_FIELD_BIT_OFFSET       = 0#UNASSIGNED_BIT           # EK: TODO:should be 0 ?
DEFAULT_FIELD_ACCESS_MODE      = 'RW'
DEFAULT_FIELD_SIDE_EFFECT      = 'NONE'
DEFAULT_FIELD_ADDRESS_OFFSET   = UNASSIGNED_ADDRESS       # EK: TODO:should be 0 ?
DEFAULT_FIELD_NUMBER_OF_FIELDS = 1
DEFAULT_FIELD_RESET_VALUE      = 0
DEFAULT_FIELD_RADIX            = 'uint32'
DEFAULT_FIELD_RESOLUTION_W     = 0           # 0 : not applicable or 2**resolution_w = 2**0 = 1
DEFAULT_FIELD_DESCRIPTION      = 'NONE'

DATA_WIDTH               = 32   # EK: TODO:purpose ? use MM_BUS_WIDTH instead ?


