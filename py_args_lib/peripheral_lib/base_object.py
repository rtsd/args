# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author  Date
#   HJ    jan 2017  Original
#   EK    feb 2017
#   PD    feb 2017
#
###############################################################################

import logging
import re
import sys
from args_errors import ARGSNameError

logger = logging.getLogger('main.base_object')

# EK: FIXME:support definition and passing on of negative parameter values, workaround:
#           . use value: 0 - 15 instead of value: -15
#           . use value: 0 - c_K instead of value: -c_K
#     alternatively:
#           . define negate(x) --> -x function in common_func.py
#           . define parameter constant c_minus_one = 0 - 1 in fpga.yaml and use c_minus_one * x --> -x
#     ==> I think it is fine to use one f these alternatives for now, because it may be difficult
#         to parse the yaml file and distinguish between - = minus and - is list element.

class BaseObject(object):
    def __init__(self):
        self.success            = True
        self._name              = ""
        self._user_defined_name = ""
        self._prefix            = ""
        self._args = {}

    def update_args(self, args):
        """ update self._args with all kv pairs in args """
        self._args.update(args)

    def _as_str(self, key, default=None):
        """ look in settings for key, if available return value as string,
        if key not available retur default """
        try:
            val = str(self._args[key])
            return val
        except KeyError:
            return default

    def _as_int(self, key, default=None):
        """ look in settings for key, if available return value as int if possible
        otherwise return string, if key not available return default """
        try:
            _val = self._args[key]
            if isinstance(_val, int):
                return self._args[key]
            return int(_val)
        except ValueError:
            return _val
        except KeyError:
            return default

    def set_kv(self, key, val):
        """ set_kv()
        if key in valid keys, update key with given value, if key not excists add it.
        """
        if val is not None:
            self._args[key] = val
            return True
        return False

    def get_kv(self, key, dtype=None):
        """ get_kv()
        return value for given key, if key in valid keys, else return None
        """
        if key not in self._args.keys():
            logger.error("key not in arguments %s", key)
            return None
        if dtype == 'int':
            return self._as_int(key)
        if dtype == 'string':
            return self._as_str(key)
        return self._args[key]

    def type(self):
        return self.get_kv('type')

    def name(self, val=None):
        """ set/get name """
        error = 0
        if val is not None:
            if isinstance(val, int):
                logger.error("BaseObject.name(), Name string '%s' evaluated to an int.", val)
                logger.error("\tAny of the following name strings: true/false/yes/no/on/off must be wrapped in quotation marks.")
                logger.error("\tName strings cannot begin with a numeric character")
                error = 1
            elif re.compile('[0-9]').match(str(val)):
                logger.error("BaseObject.name(), Name string '%s' violates ARGS naming rules. Name strings cannot begin with a numeric character.", val)
                error = 1
            elif re.compile(r'\W').search(str(val)):
                logger.error("BaseObject.name(), Name string '%s' violates ARGS naming rules. Name strings cannot contain non-alphanumeric characters.", val)
                error = 1
            if isinstance(val, str) and len(val) > 25:
                # logger.warning("BaseObject.name(), Name string \'%s\' is longer than 25 chars (%d). \nXilinx recommends 16 chars for signal and instance names, whenever possible", val, len(val))
                error = 0
            if isinstance(val, str) and len(val) < 1:
                logger.error("Name string must be at least 1 chars")
                error = 1

            if error == 1 :
                raise ARGSNameError

            self._name = val.lower()
            return
        return self._name

    def user_defined_name(self, val=None):
        """ set/get user_defined_name """
        if val is not None:
            self._user_defined_name = val
            return
        return self._user_defined_name

    def prefix(self, val=None):
        """ set/get prefix """   # EK: TODO:this prefix method can probably be removed, because no more need for structured naming.
        if val is not None:
            self._prefix = val
            return
        return self._prefix
