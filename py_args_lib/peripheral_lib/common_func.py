# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################


"""
Common definitions
"""

# System imports

import os
import math    # do not use numpy in common, to avoid making common to elaborate
import re


# Functions

def ceil_log2(num):
    """ Return integer ceil value of log2(num) """
    return int(math.ceil(math.log(int(num), 2)))


def ceil_pow2(num):
    """ Return power of 2 value that is equal or greater than num """
    return 2**ceil_log2(num)


def ceil_div(num, den):
    """ Return integer ceil value of num / den """
    return int(math.ceil( num / float(den) ) )


def smallest(a, b):
    if a<b:
        return a
    else:
        return b


def largest(a, b):
    if a>b:
        return a
    else:
        return b


def sel_a_b(sel, a, b):
    """ Return a when sel = True, else return b """
    if sel:
        return a
    else:
        return b


def unique(in_list):
    """
    Extract unique list elements (without changing the order like set() does)
    """
    seen = {}
    result = []
    for item in in_list:
        if item in seen:
            continue
        seen[item] = 1
        result.append(item)
    return result


def path_string(dir):
    joined_dir = ''.join(re.split('[/\\\\]+',dir))
    return joined_dir.lower()


def check_outdir(boardname, fpganame, args_dir=None):
    out_dir = os.path.join(os.getenv('ARGS_BUILD_DIR'), boardname.replace('uniboard', 'unb'), 'args', fpganame)
    if args_dir is not None:
        out_dir = os.path.join(out_dir, args_dir)
    try:
        os.stat(out_dir)  # Check that the output directory exists
    except FileNotFoundError:
        os.makedirs(out_dir)  # if not make it
    return out_dir
