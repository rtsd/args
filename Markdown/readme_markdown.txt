-------------------------------------------------------------------------------
--
-- Copyright 2021
-- ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
-- P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-------------------------------------------------------------------------------
--
-- Author: E. Kooistra
-- Purpose: Help for using Markdown language
--
-- Contents:
--
-- 1) Markdown viewer
-- 2) Markdown generator
-- 3) Markdown language help
--
-- References:
-- [1] Official guide: https://daringfireball.net/projects/markdown/syntax


1) Markdown viewer

Use e.g.:
- Linux 'retext' as markdown editor and previewer.
  . Use Preview menu for live preview
- https://typora.io, nice but seems too advanced and therefore not compatible
  with other viewers.
  . Markdown is a simple and not exact defined document structure language,
    therefore it seems better to keep it simple and only use what is
    supported by most viewers, e.g. by retext and the gitlab viewer.



2) Markdown generator

- Python3 --> import mdutil  # markdown writer en reader
- 1) Mark down viewer

> Python3 mdutil_example.py  # --> 1) Example_Markdown.md
> retext Example_Markdown.md



3) Markdown language help

Markdown is not well defined, so it is not always sure that the text will 
appear as expected in each viewer. For example retext and gitlab viewer differ.
Therefore it is important to keep the markdown simple and to accept that
readable text is good enough, rather than to strive for a specific layout.

Text will wrap.

Backslash is escape chararcter.

# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6

Horizontal rules three or more of ***, ___, ---

*italic*
_italic_
**bold**
__bold__
**bold and _bolditalic_**    combined
`boxed`
~~strike through~~

```vhdl
Text in ascii VHDL style for GitLab
```

Block quotes (alinea with an indent bar):
> Block text will wrap

Unordered list using *, -, +, indent >= 1 space
* Main item 1
* Main item 2          
 * sub item 2a  use 2 trailing spaces for return inside paragraph
 * sub item 2b
 
Ordered list 
1. Main item 1
2. Main item 2          
 2.1 sub item 2a
 2.2 sub item 2b
           
Images  
![Logo](path to image file)
![Logo](web link to image file)
![Logo][image1]

[image1]:web link to image file

Links:
[ASTRON]:https://www.astron.nl

Table:
|col1 | col2| Col3 |    column titles
|---|:---:|--:|    >= 3 dashes, colon for left, center, right align
| row text | row text | row text|
| row text | row text | row text|
