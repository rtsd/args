#! /usr/bin/env python3
# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author  Date
#   PD    feb 2017
#   PD    sep 2020, update and cleanup
#
###############################################################################

"""
Make automatic documentation

Usage:
> gen_doc.py -h
> gen_doc.py -f lofar2_unb2b_beamformer -v DEBUG
> gen_doc.py -f unb2b_minimal
> gen_doc.py -f lofar2_unb2b_beamformer
"""

import sys
import os
import argparse
import subprocess
import traceback
import yaml
import time
from io import StringIO
from subprocess import CalledProcessError
from pylatex import Document, Section, Subsection, Subsubsection, NewPage, Command, Package, LongTable, Tabular, Tabularx, MultiColumn, MultiRow, NewLine, MiniPage
from pylatex import SmallText, MediumText, LargeText, HugeText
from pylatex.base_classes import Environment
from pylatex.utils import italic, bold, NoEscape, verbatim, escape_latex
from copy import deepcopy
from math import ceil

from args_logger import MyLogger
from py_args_lib import *


def gen_reg_tables(subsection, group_fields):
    """
    Takes a list of fields belonging to a field group and a register subsection
    Returns the subsection with fully formatted variable width tables for a field group

    Needs to support field splitting in the case when field width exceeds 16
    """
    c_max_chars = 64
    char_sum = 0
    last_bit_boundary = 0
    bit_bounds = [32]
    i = 0
    # iterate through fields and break whenever max chars has been exceeded
    # print("New field group")
    for field in group_fields[::-1]:
        i = i + 1
        field_name = field.name() if field.group_name() is None else field.name().split(field.group_name() + '_')[-1]
        char_sum = char_sum + len(field_name)
        if char_sum > c_max_chars:
            char_sum = len(field_name)
            bit_bounds.append(last_bit_boundary)
        last_bit_boundary = field.bit_offset()
        if (bit_bounds[-1] - last_bit_boundary) > 16:
            bit_bounds.append(bit_bounds[-1] - 16)  # still limit to max of  16
            last_bit_boundary = bit_bounds[-1]
        # print("field {} upper {} lower {}".format(field.name(), str(field.bit_offset() + field.mm_width() -1), str(field.bit_offset())))
        # print(*bit_bounds)
    if bit_bounds[-1] != 0:
        bit_bounds.append(0)

    bit_bounds = [32, 16, 0] if len(bit_bounds) < 3 else bit_bounds
    nof_tables = len(bit_bounds)

    # generate columns for fields, mostly useful for gaps, can make it less bulky than the column dictionary
    for i in range(1, nof_tables):  # starts from second bound
        col_list = []
        nof_cols = bit_bounds[i - 1] - bit_bounds[i] + 1  # nof bits + bit label column
        group_table = Tabular('|c' * nof_cols + '|')
        group_table.add_hline()
        group_table.add_row((MultiColumn(nof_cols, align='|c|', data='Addr: {}h'.format(str(hex(field.address_offset()))[2:])),))
        group_table.add_hline()
        row = ['Bit'] + [str(bit) for bit in range(bit_bounds[i - 1] - 1, bit_bounds[i] - 1, -1)]
        group_table.add_row(row)
        group_table.add_hline()
        gap_bit = bit_bounds[i - 1] - 1  # next available bit, inclusive
        end_bit = bit_bounds[i]
        # print("Table {} bounding {} and {}".format(str(i),str(gap_bit + 1), str(end_bit)))
        for field in group_fields[::-1]:
            field_name = field.name() if field.group_name() is None else field.name().split(field.group_name() + '_')[-1]
            upper_bit = field.bit_offset() + field.mm_width() - 1  # inclusive
            # print("field {} has upper bit {} gap bit is {}".format(field_name,str(upper_bit), str(gap_bit)))
            if upper_bit < gap_bit:
                gap_width = min(gap_bit - upper_bit, nof_cols - 1)
                col_list.append(MultiColumn(gap_width, align='|c|', data='RES'))
                gap_bit = max(upper_bit, bit_bounds[i] - 1)  # gap_bit-(nof_cols-1))
                # print("added gap before field {} of width {}".format(field_name, str(gap_width)))
                if gap_bit == (end_bit - 1):
                    break

            # print("field {} bit offset {} should be more or equal to {} and upper bit {} should be less than {}".format(
            #       field_name, str(field.bit_offset()), str(bit_bounds[i]), str(upper_bit), str(bit_bounds[i-1])))
            if field.bit_offset() >= end_bit and upper_bit < bit_bounds[i - 1]:  # field fully contained
                col_list.append(MultiColumn(field.mm_width(), align='|c|', data=field_name))
                # print("added complete field {} of width {}".format(field_name, str(field.mm_width())))
                gap_bit = field.bit_offset() - 1
            elif upper_bit >= end_bit and field.bit_offset() < end_bit:  # upper partial
                col_list.append(MultiColumn(upper_bit - bit_bounds[i] + 1, align='|c|', data=field_name))
                # print("added upper partial for field {} of width {}".format(field_name, str(upper_bit - bit_bounds[i] + 1)))
                gap_bit = bit_bounds[i] - 1  # end of table
                break
            elif upper_bit >= bit_bounds[i - 1] and field.bit_offset() < bit_bounds[i - 1]:  # lower partial field
                col_list.append(MultiColumn(bit_bounds[i - 1] - field.bit_offset(), align='|c|', data=field_name))
                # print("added lower partial for field {} of width {}".format(field_name, str(bit_bounds[i-1]-field.bit_offset())))
                gap_bit = field.bit_offset() - 1

            if field.bit_offset() == (bit_bounds[i]):
                break

        if gap_bit != (bit_bounds[i] - 1):  # bottom gap
            gap_width = max(0, gap_bit - (bit_bounds[i] - 1))
            col_list.append(MultiColumn(gap_width, align='|c|', data='RES'))
            # print("added gap after field {} of width {}".format(field_name, str(gap_width)))
        row = ['Name'] + col_list
        group_table.add_row(row)
        group_table.add_hline()
        subsection.append(group_table)
        subsection.append(NewLine())
        subsection.append(NewLine())
        subsection.append(NewLine())

    return subsection


def main():
    # find all "*.fpga.yaml" files
    if args.fpga_name:
        fpga_lib = FPGALibrary(os.path.expandvars('$ARGS_WORK'), use_avalon_base_addr=args.use_avalon_base_addr)

    # find all "*.peripheral.yaml" files
    if args.peripheral_name:
        periph_lib = PeripheralLibrary(os.path.expandvars('$ARGS_WORK'))

    try:
        if args.fpga_name:
            doc = FPGADocumentation(args.fpga_name, fpga_lib.get_fpga(args.fpga_name))
            doc.fill()
            doc.make_pdf()
            del doc
            logger.info('Made documentation for "%s"', args.fpga_name)
            time.sleep(0.5)

        if args.peripheral_name:
            doc = PeripheralDocumentation(peripheralname, periph_lib.find_peripheral(args.peripheral_name))
            doc.fill()
            doc.make_pdf()
            del doc
            time.sleep(0.5)

    except IOError:
        logger.error("config file '{}' does not exist".format('??'))  # filename))


class FPGADocumentation(object):
    def __init__(self, fpga_name, fpga_lib):
        self.fpga_name  = fpga_name
        self.fpga = fpga_lib
        self.board_name = self.fpga.board_name

        self.out_dir = os.path.join(os.getenv('ARGS_BUILD_DIR'), self.board_name.replace('uniboard', 'unb'), 'args', self.fpga_name, 'doc')

        try:
            os.stat(self.out_dir)
        except FileNotFoundError:
            logger.debug("'%s' does not exist, making it now", self.out_dir)
            os.makedirs(self.out_dir)

        geometry_options = {"tmargin": "1cm", "lmargin": "2.5cm"}
        self.doc = Document(geometry_options=geometry_options)
        self.doc.packages.add(Package('hyperref', 'bookmarks'))  # (Command('usepackage','bookmarks','hyperref'))
        self.doc.preamble.append(Command('title', 'ARGS Documentation for FPGA design \'{}\''.format(self.fpga_name)))
        self.doc.preamble.append(Command('author', 'ARGS script gen_doc.py'))
        self.doc.preamble.append(Command('date', NoEscape(r'\today')))
        self.doc.append(NoEscape(r'\maketitle'))
        self.doc.append(NewLine())
        self.doc.append(NewLine())
        self.doc.append(bold('FPGA design description\n'))
        self.doc.append(self.fpga.fpga_description)
        self.doc.append(NewLine())
        self.doc.append(NewLine())
        self.doc.append(bold('FPGA design configuration file location\n'))
        self.doc.append(self.fpga.file_path_name.split('Firmware')[-1])
        self.doc.append(NewLine())
        self.doc.append(Command('tableofcontents'))
        self.doc.append(NewPage())

    def __del__(self):
        del self.doc
        del self.fpga
        time.sleep(0.5)

    def fill(self):
        with self.doc.create(Section("{} FPGA system map".format(self.fpga_name))):
            self.doc.append(self.fpga.fpga_description)
            self.doc.append(NewLine())
            self.doc.append(NewLine())
            self.doc.append(MediumText(bold('MM Port Map')))
            self.doc.append(NewLine())
            self.doc.append(NewLine())
            fpga_system_table = Tabular('|r|r|l|c|r|')
            fpga_system_table.add_hline()
            fpga_system_table.add_row(('Base byte address', 'Range (Bytes)', 'MM Port', 'Protocol', 'Port No.'))
            fpga_system_table.add_hline()

            for mm_port, mm_port_dict in self.fpga.address_map.items():
                fpga_system_table.add_row(('0x{:08x}'.format(mm_port_dict['base']), mm_port_dict['span'], mm_port, mm_port_dict['type'], mm_port_dict['port_index']))
                fpga_system_table.add_hline()
            self.doc.append(fpga_system_table)

        for periph_name, periph in self.fpga.peripherals.items():
            self.peripheral_fill(periph, periph_name)

    def make_pdf(self):
        stdout = sys.stdout  # keep a handle on the real standard output
        sys.stdout = StringIO()
        try:
            self.doc.generate_pdf(os.path.join(self.out_dir, self.fpga_name), clean_tex=True)
            time.sleep(0.5)
        except CalledProcessError:
            print("CalledProcessError")
            pass
        sys.stdout = stdout

    def peripheral_fill(self, periph, periph_name):
        self.doc.append(NewPage())
        periph_subsection = Section(periph_name, numbering=True)
        periph_subsection.append(periph.get_description().replace('""', ''))
        periph_reg_section = Subsection("{} register MM port".format(periph_name), numbering=False)
        periph_reg_table = Tabular('|r|r|r|l|c|')
        periph_reg_table.add_hline()
        periph_reg_table.add_row(('Base-addr', 'Addr-offset', 'Range', 'Reg-group', 'N-MM ports'))
        periph_reg_table.add_hline()
        mm_port_subsections = Subsection("MM port Ports for peripheral \'{}\'".format(periph_name), numbering=False)
        for mmport in periph.mm_ports:
            mm_port_type = 'Register block' if isinstance(mmport, Register) else 'RAM' if isinstance(mmport, RAM) else 'FIFO'
            mm_port_subsection = Subsection("MM port Port: {} ({})".format(mmport.name(), mm_port_type), numbering=True)
            mm_port_subsection.append(mmport.get_kv('mm_port_description'))
            mm_port_subsection.append(NewLine())
            mm_port_subsection.append(NewLine())
            mm_port_subsection.append("Address Length: {} bytes".format(str(mmport.address_length())))
            mm_port_subsection.append(NewLine())
            mm_port_subsection.append("Number of MM ports: {}".format(str(mmport.number_of_mm_ports())))
            mm_port_subsection.append(NewLine())
            mm_port_subsection.append(NewLine())

            if isinstance(mmport, Register):  # expand registers and fields
                periph_reg_table.add_row((mmport.base_address(), '', mmport.address_length(), mmport.name(), mmport.number_of_mm_ports()))
                periph_reg_table.add_hline()
                for ram in mmport.rams:
                    periph_reg_table.add_row(('', ram.address_offset(), ram.number_of_fields() * MM_BUS_SIZE, '>>  {} (RAM)'.format(ram.name()), mmport.number_of_mm_ports()))
                    periph_reg_table.add_hline()
                added_field_groups = []

                # generate register table i.e. by word
                group_address = -1
                group_list = []
                for field in mmport.fields:
                    if field.address_offset() != group_address:
                        group_address = field.address_offset()
                        group_list.append(field)
                c_max_rows = 30
                nof_cols = ceil(len(group_list) / c_max_rows)  # register table has max length of 40
                nof_rows = min(len(group_list), c_max_rows)
                mm_port_table = Tabular('|c|c|' * nof_cols)
                mm_port_table.add_hline()
                mm_port_table.add_row(['Hex', 'Field Group'] * nof_cols)
                mm_port_table.add_hline()
                for i in range(nof_rows):
                    row = []
                    for j in range(nof_cols):
                        if i + c_max_rows * j < len(group_list):
                            field.group_name() if field.group_name() is not None else field.name()
                            row.extend([str(hex(group_list[i + c_max_rows * j].address_offset())), group_list[i + c_max_rows * j].name()])
                        else :
                            row.extend(['', ''])
                    mm_port_table.add_row(row)
                    mm_port_table.add_hline()

                mm_port_subsection.append(mm_port_table)
                mm_port_subsection.append(NewPage())

                group_address = -1
                for field in mmport.fields:
                    if field.address_offset() != group_address:
                        group_address = field.address_offset()
                        group_subsection = Subsection('{} {}'.format(str(hex(field.address_offset())), field.name() if field.group_name() is None else field.group_name()), numbering=False)
                        group_fields = [field for field in mmport.fields if field.address_offset() == group_address]
                        if len(group_fields) > 10:
                            mm_port_subsection.append(NewPage())
                        group_subsection = gen_reg_tables(group_subsection, group_fields)
                        for field in group_fields[::-1]:
                            field_name = field.name() if field.group_name() is None else field.name().split(field.group_name() + '_')[-1]
                            bit_string = "Bit {}".format(str(field.bit_offset())) if field.mm_width() == 1 else "Bits {}:{}".format(str(field.bit_offset() + field.mm_width() - 1), str(field.bit_offset()))
                            group_subsection.append(bold("{}\t\t{} ({}):".format(bit_string, field_name, field.access_mode())))
                            group_subsection.append("\t\t{}".format(field.field_description()))
                            group_subsection.append(NewLine())
                            group_subsection.append(NewLine())
                        mm_port_subsection.append(group_subsection)
            else:  # RAM or FIFO
                mm_port_subsection.append("Data width: {}".format(mmport.mm_width()))
                mm_port_subsection.append(NewLine())
                if isinstance(mmport, RAM):
                    mm_port_subsection.append("User data width: {}".format(mmport.user_width()))

            mm_port_subsections.append(mm_port_subsection)
        periph_reg_section.append(periph_reg_table)
        self.doc.append(periph_subsection)
        if any([isinstance(mmport, Register) for mmport in periph.mm_ports]):
            self.doc.append(periph_reg_section)
        self.doc.append(mm_port_subsections)

        self.doc.append(NewPage())


class PeripheralDocumentation(object):
    def __init__(self, periph_lib_name, periph_lib):  # accepts an item from periph_libs
        self.periph_lib_name = periph_lib_name
        self.periph_lib = periph_lib

        self.out_dir = os.path.join(os.getenv('ARGS_BUILD_DIR'), self.periph_lib_name, 'doc')

        try:
            os.stat(self.out_dir)
        except FileNotFoundError:
            logger.debug("'%s' does not exist, making it now", self.out_dir)
            os.makedirs(self.out_dir)

        geometry_options = {"tmargin": "1cm", "lmargin": "2.5cm"}
        self.doc = Document(font_size='small', geometry_options=geometry_options)
        self.doc.packages.add(Package('hyperref', 'bookmarks'))
        self.doc.preamble.append(Command('title', 'ARGS Documentation for Peripheral Library \'{}\''.format(self.periph_lib_name)))
        self.doc.preamble.append(Command('author', 'ARGS script gen_doc.py'))
        self.doc.preamble.append(Command('date', NoEscape(r'\today')))
        self.doc.append(NoEscape(r'\hbadness=10000'))
        self.doc.append(NoEscape(r'\maketitle'))
        self.doc.append(NewLine())
        self.doc.append(bold('Peripheral library description\n'))
        self.doc.append(self.periph_lib['description'])
        self.doc.append(NewLine())
        self.doc.append(NewLine())
        self.doc.append(bold('Peripheral library configuration file location\n'))
        self.doc.append(self.periph_lib['file_path_name'].split('Firmware')[-1])
        self.doc.append(NewLine())
        self.doc.append(NewLine())
        self.doc.append('Note: All addressing is byte-wise')
        self.doc.append(Command('tableofcontents'))
        self.doc.append(NewPage())

    def fill(self):
        added_instances = []
        for periph_name, periph in self.periph_lib['peripherals'].items():
            if periph_name in added_instances:
                continue
            added_instances.append(periph_name)

            self.doc.append(NewPage())
            periph_subsection = Section(periph_name, numbering=True)
            periph_subsection.append(periph.get_description().replace('""', ''))

            # Peripheral System Map Table
            periph_subsection.append(NewLine())
            periph_subsection.append(NewLine())
            periph_subsection.append(MediumText(bold('Local MM port Port Map')))
            periph_subsection.append(NewLine())
            periph_subsection.append(NewLine())
            periph_system_table = Tabular('|c|c|c|c|')
            periph_system_table.add_hline()
            periph_system_table.add_row(('Hex', 'Range (Bytes)', 'MM port Port', 'Protocol'))
            periph_system_table.add_hline()

            # peripheral system address map
            dummyFPGA = FPGA(None)
            dummyFPGA.peripherals.update({periph_name: periph})
            dummyFPGA.create_address_map()
            for mm_port, mm_port_dict in dummyFPGA.address_map.items():
                periph_system_table.add_row((str(hex(mm_port_dict['base'])), str(mm_port_dict['span']), mm_port, mm_port_dict['type']))
                periph_system_table.add_hline()
            periph_subsection.append(periph_system_table)

            periph_reg_section = Subsection("{} register MM port".format(periph_name), numbering=False)
            periph_reg_table = Tabular('|c|c|c|c|')
            periph_reg_table.add_hline()
            periph_reg_table.add_row(('Base Address', 'Range', 'Register group', 'Number of MM ports'))
            periph_reg_table.add_hline()
            mm_port_subsections = Subsection("MM port Ports for peripheral \'{}\'".format(periph_name), numbering=False)
            for mmport in periph.mm_ports:
                mm_port_subsection = Subsection("MM port Port: {} ({})".format(mmport.name(), 'Register block' if isinstance(mmport, Register) else 'RAM' if isinstance(mmport, RAM) else 'FIFO'), numbering=True)
                mm_port_subsection.append(mmport.get_kv('mm_port_description'))
                mm_port_subsection.append(NewLine())
                mm_port_subsection.append(NewLine())
                mm_port_subsection.append("Address Length: {}".format(str(mmport.address_length())))
                mm_port_subsection.append(NewLine())
                mm_port_subsection.append("Number of MM ports: {}".format(str(mmport.number_of_mm_ports())))
                mm_port_subsection.append(NewLine())
                mm_port_subsection.append(NewLine())

                if isinstance(mmport, Register):  # expand registers and fields
                    for ram in mmport.rams:
                        periph_reg_table.add_row((str(ram.base_address()), str(ram.number_of_fields() * MM_BUS_SIZE), ram.name() + ' (RAM)', str(mmport.number_of_mm_ports())))
                        periph_reg_table.add_hline()
                    periph_reg_table.add_row((str(mmport.base_address()), str(mmport.address_length()), mmport.name(), str(mmport.number_of_mm_ports())))
                    periph_reg_table.add_hline()
                    added_field_groups = []

                    # generate register table i.e. by word
                    group_address = -1
                    group_list = []
                    for field in mmport.fields:
                        if field.address_offset() != group_address:
                            group_address = field.address_offset()
                            group_list.append(field)

                    c_max_rows = 30
                    nof_cols = ceil(len(group_list) / c_max_rows)  # register table has max length of c_max_rows
                    nof_rows = min(len(group_list), c_max_rows)
                    mm_port_table = Tabular('|c|c|' * nof_cols)
                    mm_port_table.add_hline()
                    mm_port_table.add_row(['Hex', 'Field Group'] * nof_cols)
                    mm_port_table.add_hline()
                    for i in range(nof_rows):
                        row = []
                        for j in range(nof_cols):
                            if i + c_max_rows * j < len(group_list):
                                field.group_name() if field.group_name() is not None else field.name()
                                row.extend([str(hex(group_list[i + c_max_rows * j].address_offset())), group_list[i + c_max_rows * j].name()])
                            else :
                                row.extend(['', ''])
                        mm_port_table.add_row(row)
                        mm_port_table.add_hline()

                    mm_port_subsection.append(mm_port_table)
                    mm_port_subsection.append(NewPage())

                    group_address = -1
                    for field in mmport.fields:
                        if field.address_offset() != group_address:
                            group_address = field.address_offset()
                            group_subsection = Subsection('{} {}'.format(str(hex(field.address_offset())), field.name() if field.group_name() is None else field.group_name()), numbering=False)
                            group_fields = [field for field in mmport.fields if field.address_offset() == group_address]
                            if len(group_fields) > 10:
                                mm_port_subsection.append(NewPage())
                            group_subsection = gen_reg_tables(group_subsection, group_fields)
                            for field in group_fields[::-1]:
                                field_name = field.name() if field.group_name() is None else field.name().split(field.group_name() + '_')[-1]
                                bit_string = "Bit {}".format(str(field.bit_offset())) if field.mm_width() == 1 else "Bits {}:{}".format(str(field.bit_offset() + field.mm_width() - 1), str(field.bit_offset()))
                                group_subsection.append(bold("{}\t\t{} ({}):".format(bit_string, field_name, field.access_mode())))
                                group_subsection.append("\t\t{}".format(field.field_description()))
                                group_subsection.append(NewLine())
                                group_subsection.append(NewLine())
                            mm_port_subsection.append(group_subsection)
                else:  # RAM or FIFO
                    mm_port_subsection.append("Data width: {}".format(mmport.mm_width()))
                    mm_port_subsection.append(NewLine())
                    if isinstance(mmport, RAM):
                        mm_port_subsection.append("User data width: {}".format(mmport.user_width()))

                mm_port_subsections.append(mm_port_subsection)
            periph_reg_section.append(periph_reg_table)
            self.doc.append(periph_subsection)
            if any([isinstance(mmport, Register) for mmport in periph.mm_ports]):
                self.doc.append(periph_reg_section)
            self.doc.append(mm_port_subsections)

            self.doc.append(NewPage())

    def make_pdf(self):
        stdout = sys.stdout  # keep a handle on the real standard output
        sys.stdout = StringIO()
        try:
            self.doc.generate_pdf(os.path.join(self.out_dir, self.periph_lib_name), clean_tex=True)
            time.sleep(0.5)
        except CalledProcessError:
            pass
        sys.stdout = stdout


if __name__ == "__main__":
    # Parse command line arguments
    parser = argparse.ArgumentParser(description="System and peripheral config command line parser arguments")
    parser.add_argument('-f', '--fpga', dest='fpga_name', type=str, help="fpga system name")
    parser.add_argument('-a', '--avalon', dest='use_avalon_base_addr', action='store_true', default=False, help="use avalon base addresses")
    parser.add_argument('-p', '--peripheral', dest='peripheral_name', type=str, help="peripheral name")
    parser.add_argument('-v', '--verbosity', default='INFO', help="stdout log level can be [ERROR | WARNING | INFO | DEBUG]")
    args = parser.parse_args()

    # setup first log system before importing other user libraries
    program_name = __file__.split('/')[-1].split('.')[0]
    out_dir = os.path.join(os.getenv('ARGS_GEAR'), 'log')
    my_logger = MyLogger(log_path=out_dir, file_name=program_name)
    my_logger.set_file_log_level('DEBUG')
    my_logger.set_stdout_log_level(args.verbosity)

    logger = my_logger.get_logger()
    logger.debug("Used arguments: {}".format(args))

    if not args.peripheral_name and not args.fpga_name:
        parser.print_help()
        sys.exit(1)
    try:
        main()
    except (ARGSNameError, ARGSModeError, ARGSYamlError):
        handle_args_error(sys.exc_info()[0])
    except:
        logger.error('Program fault, reporting and cleanup')
        logger.error('Caught %s', str(sys.exc_info()[0]))
        logger.error(str(sys.exc_info()[1]))
        logger.error('TRACEBACK:\n%s', traceback.format_exc())
        logger.error('Aborting NOW')
        sys.exit("ERROR")

    sys.exit("Normal Exit")
