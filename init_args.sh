#!/usr/bin/env bash -e
###############################################################################
#
# Copyright (C) 2018 
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
#
###############################################################################

#
# Initialisation script to setup the environment variables for args
#

# 
# Make sure it is sourced and no one accidentally gave the script execution rights and just executes it.
if [[ "$_" == "${0}" ]]; then
    echo "ERROR: Use this command with '. ' or 'source '"
    sleep 1
    exit
fi

# check for ARGS_WORK variable , if it not exists:
# 1) run ". ./init_hdl.sh" in the hdl source dir instead.
# 2) or set variable manualy

if [ -z "${ARGS_WORK}" ]; then
    echo "environ variable 'ARGS_WORK' not availble"
    echo "source 'init_hdl.sh' script in hdl directory instead of this one"
    echo ". ./init_hdl.sh"
    exit
fi

echo "ARGS environment will be setup for" $ARGS_WORK

# Figure out where this script is located and set environment variables accordingly
export ARGS_GEAR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# setup paths to build and config dir if not already defined by the user.
export ARGS_BUILD_DIR=${ARGS_WORK}/build

if [[ ! -d "${ARGS_BUILD_DIR}" ]]; then
    echo "make args dir"
    mkdir ${ARGS_BUILD_DIR}
fi

# Define function to add directories to a given environment variable
#     args: name_of_env_var new_path [new_path ...]
# Directories are only added when they exist.
pathadd() {
    for new_dir in ${@:2}
    do
        eval dir_to_add=`echo ${new_dir}`
        if [ -d ${dir_to_add} ] && ! echo ${!1} | grep -E -q "(^|:)$dir_to_add($|:)" ; then
            eval export ${1}=${1:+${!1#:}:}${dir_to_add}
        fi
    done
}

# Extend the PATH and PYTHONPATH variables
pathadd "PATH" ${ARGS_GEAR}
